﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public class RectangularWindow : AbstractWindow
    {
        public RectangularWindow(int count) : base(count)
        {
        }

        public override double Calculate(int index, double value)
        {
            return value;
        }
    }
}
