﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public abstract class AbstractWindow
    {
        public int SampleCount { get; private set; }

        public AbstractWindow(int count)
        {
            SampleCount = count;
        }

        public abstract double Calculate(int index, double value);
    }
}
