﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public class BlackmanWindow : AbstractWindow
    {
        public BlackmanWindow(int count) : base(count)
        {
        }

        public override double Calculate(int index, double value)
        {
            double factor = 0.42 - 0.50 * Math.Cos(2 * Math.PI * index / SampleCount - 1) + 0.08 * Math.Cos(4 * Math.PI * index / SampleCount - 1);
            return value * factor;
        }
    }
}
