﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public class HannWindow : AbstractWindow
    {
        public HannWindow(int count) : base(count)
        {
        }

        public override double Calculate(int index, double value)
        {
            double factor = (1 - Math.Cos(2 * Math.PI * index / SampleCount - 1)) / 2f;
            return value * factor;
        }
    }
}
