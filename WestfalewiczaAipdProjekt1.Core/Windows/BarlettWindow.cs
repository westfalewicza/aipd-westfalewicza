﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public class BarlettWindow : AbstractWindow
    {
        public BarlettWindow(int count) : base(count)
        {
        }

        public override double Calculate(int index, double value)
        {
            float N1 = SampleCount - 1;
            double factor = 1 - (2 * Math.Abs(index - N1 / 2f) / N1);
            return value * factor;
        }
    }
}
