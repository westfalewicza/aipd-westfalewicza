﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Windows
{
    public class HammingWindow : AbstractWindow
    {
        public HammingWindow(int count) : base(count)
        {
        }

        public override double Calculate(int index, double value)
        {
            double factor = 0.54 - 0.46 * Math.Cos(2 * Math.PI * index / SampleCount - 1);
            return value * factor;
        }
    }
}
