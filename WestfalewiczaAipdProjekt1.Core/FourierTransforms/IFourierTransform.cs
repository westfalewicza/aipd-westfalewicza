﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.FourierTransforms
{
    public interface IFourierTransform
    {
        Complex[] FFT(Complex[] input);
    }
}
