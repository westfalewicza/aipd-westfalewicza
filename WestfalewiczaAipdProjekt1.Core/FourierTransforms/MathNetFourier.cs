﻿using MathNet.Numerics.IntegralTransforms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.FourierTransforms
{
    public class MathNetFourier : IFourierTransform
    {
        public Complex[] FFT(Complex[] input)
        {
            var output = new Complex[input.Length];
            input.CopyTo(output, 0);
            Fourier.Forward(output);
            return output;
        }
    }
}
