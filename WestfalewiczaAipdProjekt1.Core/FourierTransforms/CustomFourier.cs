﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.FourierTransforms;

namespace WestfalewiczaAipdProjekt1.Core.FourierTransforms
{
    public class CustomFourier : IFourierTransform
    {
        public Complex[] FFT(Complex[] input)
        {
            if (input.Length == 1)
            {
                return new Complex[] { input[0] };
            }

            int N = input.Length;
            Complex[] X = new Complex[N];
            Complex[] e = new Complex[N / 2];
            Complex[] d = new Complex[N / 2];
            for (int k = 0; k < N / 2; k++)
            {
                e[k] = input[2 * k];
                d[k] = input[2 * k + 1];
            }
            Complex[] D = FFT(d);
            Complex[] E = FFT(e);

            for (int k = 0; k < N / 2; k++)
            {
                Complex temp = FromRadian(1, -2 * Math.PI * k / N);
                D[k] *= temp;
            }
            for (int k = 0; k < N / 2; k++)
            {
                X[k] = E[k] + D[k];
                X[k + N / 2] = E[k] - D[k];
            }
            return X;
        }

        private static Complex FromRadian(double r, double angle)
        {
            return new Complex(r * Math.Cos(angle), r * Math.Sin(angle));
        }
    }
}