﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core
{
    public static class Config
    {
        public static int BottomFrequency = 15;
        public static int TopFrequency = 22_000;
        public static double PracticallyZeroFrequency = 0.001d;

        public static int CepstrumBottomFrequency = 40;
        public static int CepstrumTopFrequency = 500;
        public static int CepstrumFrameLength = 4096;

        public static int DefaultFrameLength = 256;
        public static int ClipLength = 100;

        public static float UnvoicedZcrThreshold = 0.080f;
        public static float UnvoicedSteThreshold = 0.014f;
        public static float UnvoicedZcr2SteThreshold = 9f;

        public static float VoicedZcrThreshold = 0.1f;
        public static float VoicedSteThreshold = 0.01f;

        public static float SilenceZcrThreshold = 0.05f;
        public static float SilenceSteThreshold = 0.004f;

        public static float MusicMlerThreshold = 0.045f;
        public static float MusicSdzcrThreshold = 0.045f;

        public static float SpeechMlerThresholdDown = 0.1f;
        public static float SpeechMlerThresholdUp = 0.6f;
        public static float SpeechSdzcrThreshold = 0.05f;
    }
}
