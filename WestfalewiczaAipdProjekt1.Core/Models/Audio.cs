﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class Audio
    {
        public string Filename { get; set; }
        public WaveFormat WaveFormat { get; set; }
        public List<float> Left { get; set; }
        public List<float> Right { get; set; }
        public float Time { get; set; }

        public Audio(string filename, WaveFormat waveFormat, List<float> left, List<float> right, float time)
        {
            Filename = filename;
            WaveFormat = waveFormat;
            Left = left;
            Right = right;
            Time = time;
        }

        protected Audio(Audio audio)
        {
            Filename = audio.Filename;
            WaveFormat = audio.WaveFormat;
            Left = audio.Left;
            Right = audio.Right;
            Time = audio.Time;
        }
    }
}
