﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class AudioClip
    {
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public float Start { get; set; }
        public float End { get; set; }

        public float Mler { get; set; }
        public float Sdzcr { get; set; }
        public AudioClipType Type { get; set; }
    }
}
