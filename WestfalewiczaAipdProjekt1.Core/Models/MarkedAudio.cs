﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class MarkedAudio : Audio
    {
        public List<AudioFrame> LeftProcessed { get; }
        public List<AudioFrame> RightProcessed { get; }

        public MarkedAudio(List<AudioFrame> leftProcessed, List<AudioFrame> rightProcessed, Audio audio) : base(audio)
        {
            LeftProcessed = leftProcessed;
            RightProcessed = rightProcessed;
        }

        public MarkedAudio(Audio audio) : base(audio)
        {
            LeftProcessed = new List<AudioFrame>();
            RightProcessed = new List<AudioFrame>();
        }

        protected MarkedAudio(MarkedAudio audio) : base(audio)
        {
            LeftProcessed = audio.LeftProcessed;
            RightProcessed = audio.RightProcessed;
        }
    }
}
