﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class RandomTimeAccessAudio : Audio
    {
        public RandomTimeAccessAudio(Audio audio) : base(audio)
        {
        }

        public (IEnumerable<AudioFrame>, IEnumerable<AudioFrame>) Frames(double from, double seconds, double overlay, int frameLength)
        {
            int startIndex = (int)from * WaveFormat.SampleRate;
            int endIndex = (int)(from + seconds) * WaveFormat.SampleRate;
            int step = (int)(frameLength * (1d - overlay / 100d));
            return (DoFrames(startIndex, endIndex, step, frameLength, Left), DoFrames(startIndex, endIndex, step, frameLength, Right));
        }

        private IEnumerable<AudioFrame> DoFrames(int startIndex, int endIndex, int step,int frameLength, List<float> data)
        {
            for (int i = startIndex; i < endIndex; i += step)
            {
                int startI = i;
                var list = new List<float>();
                for (int n = 0; n < frameLength; n++)
                {
                    if (i + n >= data.Count)
                    {
                        yield break;
                    }

                    list.Add(data[i+n]);
                }
                yield return new AudioFrame()
                {
                    Data = list,
                    StartIndex = startI,
                    EndIndex = startI + frameLength,
                    Start = (float)startI / WaveFormat.SampleRate,
                    End = (float)(startI + frameLength) / WaveFormat.SampleRate,
                };
            }
        }
    }
}
