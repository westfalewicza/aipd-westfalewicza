﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WestfalewiczaAipdProjekt1.Core.Models
{
    public class CondencedAudio : MarkedAudio
    {
        public List<AudioClip> LeftClips { get; set; }
        public List<AudioClip> RightClips { get; set; }

        public CondencedAudio(MarkedAudio marked) : base(marked)
        {
            LeftClips = new List<AudioClip>();
            RightClips = new List<AudioClip>();
        }
    }
}
