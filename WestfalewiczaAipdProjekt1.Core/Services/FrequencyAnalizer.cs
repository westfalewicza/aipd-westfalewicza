﻿using MathNet.Numerics.IntegralTransforms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.FourierTransforms;
using WestfalewiczaAipdProjekt1.Core.Models;
using WestfalewiczaAipdProjekt1.Core.Windows;

namespace WestfalewiczaAipdProjekt1.Core.Services
{
    public class FrequencyAnalizer
    {
        public Dictionary<double, List<Complex>> ProcessFFT(List<AudioFrame> frames, int sampleRate, IFourierTransform fourier, AbstractWindow window)
        {
            if (frames == null || !frames.Any())
                throw new ArgumentNullException();

            var frequencies = Fourier.FrequencyScale(frames.First().Data.Count, sampleRate);
            var values = new Dictionary<double, List<Complex>>();
            foreach (var freq in frequencies.Where(f => Config.BottomFrequency < f && f < Config.TopFrequency))
            {
                values.Add(freq, new List<Complex>());
            }

            foreach (AudioFrame frame in frames)
            {
                Complex[] samples = new Complex[frame.Data.Count];
                for (int i = 0; i < frame.Data.Count; i++)
                    samples[i] = new Complex(window.Calculate(i, frame.Data[i]), 0);

                samples = fourier.FFT(samples);

                for (int i = 0; i < samples.Length; i++)
                {
                    if (Config.BottomFrequency < frequencies[i] && frequencies[i] < Config.TopFrequency)
                    {
                        values[frequencies[i]].Add(samples[i]);
                    }
                }
            }

            return values;
        }

        public Dictionary<double, double> ProcessCepstrum(List<AudioFrame> frames, int sampleRate, IFourierTransform fourier)
        {
            if (frames == null || !frames.Any())
                throw new ArgumentNullException();

            var output = new Dictionary<double, double>();
            var frequencies = Fourier.FrequencyScale(frames.First().Data.Count, sampleRate);
            foreach (AudioFrame frame in frames)
            {
                if (frame.FrameType != AudioFrameType.Voiced)
                    continue;

                Complex[] samples = new Complex[frame.Data.Count];
                for (int i = 0; i < frame.Data.Count; i++)
                    samples[i] = new Complex(frame.Data[i], 0);

                samples = fourier.FFT(samples);

                var reals = samples.Select(c => new Complex(Math.Log(c.Magnitude), 0)).ToArray();

                Fourier.Inverse(reals);

                int maxIndex = 0;
                double max = double.MinValue;
                for (int i = 0; i < reals.Length; i++)
                {
                    if (frequencies[i] > Config.CepstrumBottomFrequency
                        && frequencies[i] < Config.CepstrumTopFrequency
                        && max < reals[i].Magnitude)
                    {
                        maxIndex = i;
                        max = reals[i].Magnitude;
                    }
                }

                if(!output.ContainsKey(Math.Round(frame.Start, 3)))
                    output.Add(Math.Round(frame.Start, 3), frequencies[maxIndex]);
                else
                    output.Add(frame.Start, frequencies[maxIndex]);
            }
            return output;
        }

        public List<Complex[]> ProcessSpectogram(List<AudioFrame> frames, int sampleRate, AbstractWindow window, IFourierTransform fourier)
        {
            if (frames == null || !frames.Any())
                throw new ArgumentNullException();

            var frequencies = Fourier.FrequencyScale(frames.First().Data.Count, sampleRate);

            var filteredFrequenciesN = frequencies.Where(f => Config.BottomFrequency < f && f < Config.TopFrequency).Count();
            List<Complex[]> transformed = new List<Complex[]>(frames.Count);

            for (int frameNumber = 0; frameNumber < frames.Count; frameNumber++)
            {
                var frame = frames[frameNumber];
                Complex[] samples = new Complex[frame.Data.Count];
                for (int i = 0; i < frame.Data.Count; i++)
                    samples[i] = new Complex(window.Calculate(i, frame.Data[i]), 0);

                samples = fourier.FFT(samples);

                var values = new Complex[filteredFrequenciesN];
                int j = 0;
                for (int i = 0; i < samples.Length; i++)
                {
                    if (Config.BottomFrequency < frequencies[i] && frequencies[i] < Config.TopFrequency)
                    {
                        values[j] = samples[i];
                        j++;
                    }
                }
                transformed.Add(values);
            }
            return transformed;
        }
    }
}
