﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.Models;
using WestfalewiczaAipdProjekt1.Core.Utils;

namespace WestfalewiczaAipdProjekt1.Core.Services
{
    public class AudioCondenser
    {
        public CondencedAudio Condence(MarkedAudio audio)
        {
            var xd = new CondencedAudio(audio);
            DoCondence(audio.LeftProcessed, xd.LeftClips);
            DoCondence(audio.RightProcessed, xd.RightClips);

            return xd;
        }

        private void DoCondence(List<AudioFrame> processed, List<AudioClip> audioClips)
        {
            int step = 44100 / 256;
            for (int i = 0; i < processed.Count; i += step)
            {
                List<AudioFrame> framesInClip;
                if (i + step < processed.Count)
                    framesInClip = processed.Skip(i).Take(step).ToList();
                else
                    framesInClip = processed.Skip(i).ToList();

                var xd = new AudioClip()
                {
                    StartIndex = i,
                    EndIndex = i + step < processed.Count ? i + step : processed.Count - 1,
                    Start = processed[i].Start,
                    End = processed[i + step < processed.Count ? i + step : processed.Count - 1].End,
                    Mler = BasicProcessing.GetMLER(framesInClip),
                    Sdzcr = BasicProcessing.GetSDZCR(framesInClip)
                };
                FillType(xd);
                audioClips.Add(xd);
            }
        }

        private void FillType(AudioClip clip)
        {
            if (clip.Mler <= Config.MusicMlerThreshold)
            {
                clip.Type = AudioClipType.Music;
                return;
            }
            if (clip.Mler >= Config.SpeechMlerThresholdDown && clip.Mler <= Config.SpeechMlerThresholdUp)
            {
                clip.Type = AudioClipType.Speech;
                return;
            }
            if (clip.Sdzcr <= Config.MusicSdzcrThreshold)
            {
                clip.Type = AudioClipType.Music;
                return;
            }
            if (clip.Sdzcr >= Config.SpeechSdzcrThreshold)
            {
                clip.Type = AudioClipType.Speech;
                return;
            }

            clip.Type = AudioClipType.None;
        }
    }
}
