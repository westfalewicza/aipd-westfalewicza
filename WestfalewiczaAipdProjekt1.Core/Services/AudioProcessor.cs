﻿using NAudio.Wave;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.Models;
using WestfalewiczaAipdProjekt1.Core.Utils;

namespace WestfalewiczaAipdProjekt1.Core.Services
{
    public class AudioProcessor
    {
        public MarkedAudio Process(Audio audio)
        {
            int frameLength = Config.DefaultFrameLength;
            while (audio.Left.Count / frameLength > 5_000)
                frameLength = frameLength * 2;

            ConcurrentBag<AudioFrame> leftProcessed = new ConcurrentBag<AudioFrame>();
            ConcurrentBag<AudioFrame> rightProcessed = new ConcurrentBag<AudioFrame>();
            int estimateCount = (int)Math.Ceiling((decimal)audio.Left.Count / frameLength);

            Parallel.For(0, estimateCount, index =>
            {
                int start = index * frameLength;
                int end = start + frameLength - 1 > audio.Left.Count - 1 ? audio.Left.Count - 1 : start + frameLength - 1;
                AudioFrame LeftChunk = CreateFrame(frameLength, audio.Left, start, end, audio.WaveFormat.SampleRate);
                AudioFrame RightChunk = CreateFrame(frameLength, audio.Right, start, end, audio.WaveFormat.SampleRate);
                rightProcessed.Add(RightChunk);
                leftProcessed.Add(LeftChunk);
            });

            var xd = new MarkedAudio(leftProcessed.OrderBy(af => af.StartIndex).ToList(), rightProcessed.OrderBy(af => af.StartIndex).ToList(), audio);

            return xd;
        }

        private AudioFrame CreateFrame(int frameLength, List<float> data, int start, int end, int sampleRate)
        {
            List<float> chunkData = new List<float>(end - start + 1);
            for (int i = start; i <= end; i++)
            {
                chunkData.Add(data[i]);
            }

            AudioFrame frame = new AudioFrame
            {
                StartIndex = start,
                EndIndex = end,
                Start = (float)start / sampleRate,
                End = (float)end / sampleRate,
                Data = chunkData
            };
            frame.Analize();

            return frame;
        }



    }
}
