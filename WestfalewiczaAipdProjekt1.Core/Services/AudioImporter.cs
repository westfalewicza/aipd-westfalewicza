﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.Models;

namespace WestfalewiczaAipdProjekt1.Core.Services
{
    public class AudioImporter
    {

        public Audio Import(string filename)
        {
            var reader = new AudioFileReader(filename);
            
            float[] buffer = new float[reader.Length / 4];

            int alreadyRead = 0;
            while ((alreadyRead = reader.Read(buffer, 0, buffer.Length)) != 0) ;

            List<float> leftChannel;
            List<float> rightChannel;

            if (reader.WaveFormat.Channels == 1)
            {
                leftChannel = buffer.Select(x=>x).ToList();
                rightChannel = buffer.Select(x => x).ToList();

            }
            else
            {
                leftChannel = buffer.Where((x, index) => index % 2 == 0).ToList();
                rightChannel = buffer.Where((x, index) => index % 2 == 1).ToList();
            }

            var xd = new Audio(
                filename,
                reader.WaveFormat,
                leftChannel,
                rightChannel,
                (float)leftChannel.Count / reader.WaveFormat.SampleRate
            );
            return xd;
        }
    }
}
