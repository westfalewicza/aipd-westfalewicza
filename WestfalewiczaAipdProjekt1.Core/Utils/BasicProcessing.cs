﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WestfalewiczaAipdProjekt1.Core.Models;

namespace WestfalewiczaAipdProjekt1.Core.Utils
{
    public static class BasicProcessing
    {
        public static float GetMLER(List<AudioFrame> frames)
        {
            float avg = frames.Average(x => x.Ste);
            float sum = frames.Sum(x => Math.Sign(0.2f * avg - x.Ste) + 1);

            return 0.5f * sum / frames.Count;
        }

        public static float GetSDZCR(List<AudioFrame> frames)
        {
            float avg = frames.Average(x => x.Zcr);
            float sum = frames.Sum(x => (x.Zcr - avg) * (x.Zcr - avg));

            return (float)Math.Sqrt(sum / frames.Count);
        }

        public static float GetZCR(List<float> data)
        {
            float signChangeAcc = 0f;
            for (int i = 1; i < data.Count; i++)
            {
                signChangeAcc += Math.Abs(Math.Sign(data[i]) - Math.Sign(data[i - 1]));
            }
            return signChangeAcc * 0.5f / data.Count;
        }

        public static float GetSTE(List<float> data)
        {
            return (data.Sum(x => x * x) / data.Count);
        }
    }
}
