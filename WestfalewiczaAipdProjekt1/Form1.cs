﻿using MathNet.Numerics.IntegralTransforms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WestfalewiczaAipdProjekt1.Core;
using WestfalewiczaAipdProjekt1.Core.Models;
using WestfalewiczaAipdProjekt1.Core.Services;
using WestfalewiczaAipdProjekt1.Core.Windows;

namespace WestfalewiczaAipdProjekt1
{
    public partial class Form1 : Form
    {
        public CondencedAudio Audio { get; set; }

        public AudioDomain SelectedDomain { get; set; }

        public Form1()
        {
            InitializeComponent();
            SelectedDomain = AudioDomain.None;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            timeToolStripMenuItem_Click(null, null);
        }


        private void timeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedDomain == AudioDomain.Time)
                return;
            SelectedDomain = AudioDomain.Time;

            SpectogramDomain.Hide();
            FrequencyDomain.Hide();
            CepstrumDomain.Hide();
            TimeDomain.Show();
            TimeDomain.Process();
        }
        private void frequencyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedDomain == AudioDomain.Frequency)
                return;

            SelectedDomain = AudioDomain.Frequency;
            SpectogramDomain.Hide();
            TimeDomain.Hide();
            CepstrumDomain.Hide();
            FrequencyDomain.Show();
            FrequencyDomain.Process();
        }
        private void spectogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedDomain == AudioDomain.Spectogram)
                return;

            SelectedDomain = AudioDomain.Spectogram;
            TimeDomain.Hide();
            FrequencyDomain.Hide();
            CepstrumDomain.Hide();
            SpectogramDomain.Show();
            SpectogramDomain.Process();
        }
        private void cepstrumToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (SelectedDomain == AudioDomain.Cepstrum)
                return;

            SelectedDomain = AudioDomain.Cepstrum;
            TimeDomain.Hide();
            FrequencyDomain.Hide();
            SpectogramDomain.Hide();
            CepstrumDomain.Show();
            CepstrumDomain.Process();
        }
        
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FileNameInMenu.Text = "LOADING";

                var worker = new BackgroundWorker();
                worker.DoWork += (snd, arg) =>
                {
                    var xd1 = new AudioImporter();
                    var dx1 = xd1.Import(openFileDialog1.FileName);
                    var xd2 = new AudioProcessor();
                    var dx2 = xd2.Process(dx1);
                    var xd3 = new AudioCondenser();
                    Audio = xd3.Condence(dx2);
                };
                worker.RunWorkerCompleted += (snd, arg) =>
                {
                    FileNameInMenu.Text = "Loaded: " + openFileDialog1.FileName;
                    FrequencyDomain.Audio = Audio;
                    TimeDomain.Audio = Audio;
                    SpectogramDomain.Audio = Audio;
                    CepstrumDomain.Audio = Audio;
                    SelectedDomain = AudioDomain.None;
                    timeToolStripMenuItem_Click(null, null);
                };
                worker.RunWorkerAsync();

            }
        }
    }
}
