﻿namespace WestfalewiczaAipdProjekt1.Controls
{
    partial class FrequencyDomainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.FrequencyDomainContainer = new System.Windows.Forms.Panel();
            this.FrequencyBottomPanel = new System.Windows.Forms.Panel();
            this.FrequencyWindowBox = new System.Windows.Forms.GroupBox();
            this.BarlettWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.BlackmanWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HammingWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HannWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.RectangularWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.FrequencyGraphSettings = new System.Windows.Forms.Panel();
            this.FrequencyAnalizingWindowSettings = new System.Windows.Forms.GroupBox();
            this.AnalizingWindowButton = new System.Windows.Forms.Button();
            this.FrequencyGraphForLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFromLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFrom = new System.Windows.Forms.TextBox();
            this.FrequencyGraphFor = new System.Windows.Forms.TextBox();
            this.FrequencyChartsContainer = new System.Windows.Forms.Panel();
            this.FrequencyLeftChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FrequencyRightChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FrequencyDomainContainer.SuspendLayout();
            this.FrequencyBottomPanel.SuspendLayout();
            this.FrequencyWindowBox.SuspendLayout();
            this.FrequencyGraphSettings.SuspendLayout();
            this.FrequencyAnalizingWindowSettings.SuspendLayout();
            this.FrequencyChartsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyLeftChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyRightChart)).BeginInit();
            this.SuspendLayout();
            // 
            // FrequencyDomainContainer
            // 
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyBottomPanel);
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyGraphSettings);
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyChartsContainer);
            this.FrequencyDomainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrequencyDomainContainer.Location = new System.Drawing.Point(0, 0);
            this.FrequencyDomainContainer.Name = "FrequencyDomainContainer";
            this.FrequencyDomainContainer.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyDomainContainer.TabIndex = 6;
            // 
            // FrequencyBottomPanel
            // 
            this.FrequencyBottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrequencyBottomPanel.Controls.Add(this.FrequencyWindowBox);
            this.FrequencyBottomPanel.Location = new System.Drawing.Point(0, 500);
            this.FrequencyBottomPanel.Name = "FrequencyBottomPanel";
            this.FrequencyBottomPanel.Size = new System.Drawing.Size(1268, 359);
            this.FrequencyBottomPanel.TabIndex = 7;
            // 
            // FrequencyWindowBox
            // 
            this.FrequencyWindowBox.Controls.Add(this.BarlettWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.BlackmanWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HammingWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HannWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.RectangularWindowRadioButton);
            this.FrequencyWindowBox.Location = new System.Drawing.Point(22, 15);
            this.FrequencyWindowBox.Name = "FrequencyWindowBox";
            this.FrequencyWindowBox.Size = new System.Drawing.Size(200, 202);
            this.FrequencyWindowBox.TabIndex = 0;
            this.FrequencyWindowBox.TabStop = false;
            this.FrequencyWindowBox.Text = "Selection Window";
            // 
            // BarlettWindowRadioButton
            // 
            this.BarlettWindowRadioButton.AutoSize = true;
            this.BarlettWindowRadioButton.Location = new System.Drawing.Point(6, 129);
            this.BarlettWindowRadioButton.Name = "BarlettWindowRadioButton";
            this.BarlettWindowRadioButton.Size = new System.Drawing.Size(119, 21);
            this.BarlettWindowRadioButton.TabIndex = 4;
            this.BarlettWindowRadioButton.Text = "Barlett window";
            this.BarlettWindowRadioButton.UseVisualStyleBackColor = true;
            this.BarlettWindowRadioButton.CheckedChanged += new System.EventHandler(this.BarlettWindowRadioButton_CheckedChanged);
            // 
            // BlackmanWindowRadioButton
            // 
            this.BlackmanWindowRadioButton.AutoSize = true;
            this.BlackmanWindowRadioButton.Location = new System.Drawing.Point(6, 102);
            this.BlackmanWindowRadioButton.Name = "BlackmanWindowRadioButton";
            this.BlackmanWindowRadioButton.Size = new System.Drawing.Size(139, 21);
            this.BlackmanWindowRadioButton.TabIndex = 3;
            this.BlackmanWindowRadioButton.Text = "Balckman window";
            this.BlackmanWindowRadioButton.UseVisualStyleBackColor = true;
            this.BlackmanWindowRadioButton.CheckedChanged += new System.EventHandler(this.BlackmanWindowRadioButton_CheckedChanged);
            // 
            // HammingWindowRadioButton
            // 
            this.HammingWindowRadioButton.AutoSize = true;
            this.HammingWindowRadioButton.Location = new System.Drawing.Point(6, 75);
            this.HammingWindowRadioButton.Name = "HammingWindowRadioButton";
            this.HammingWindowRadioButton.Size = new System.Drawing.Size(137, 21);
            this.HammingWindowRadioButton.TabIndex = 2;
            this.HammingWindowRadioButton.Text = "Hamming window";
            this.HammingWindowRadioButton.UseVisualStyleBackColor = true;
            this.HammingWindowRadioButton.CheckedChanged += new System.EventHandler(this.HammingWindowRadioButton_CheckedChanged);
            // 
            // HannWindowRadioButton
            // 
            this.HannWindowRadioButton.AutoSize = true;
            this.HannWindowRadioButton.Location = new System.Drawing.Point(6, 48);
            this.HannWindowRadioButton.Name = "HannWindowRadioButton";
            this.HannWindowRadioButton.Size = new System.Drawing.Size(112, 21);
            this.HannWindowRadioButton.TabIndex = 1;
            this.HannWindowRadioButton.Text = "Hann window";
            this.HannWindowRadioButton.UseVisualStyleBackColor = true;
            this.HannWindowRadioButton.CheckedChanged += new System.EventHandler(this.HannWindowRadioButton_CheckedChanged);
            // 
            // RectangularWindowRadioButton
            // 
            this.RectangularWindowRadioButton.AutoSize = true;
            this.RectangularWindowRadioButton.Checked = true;
            this.RectangularWindowRadioButton.Location = new System.Drawing.Point(6, 21);
            this.RectangularWindowRadioButton.Name = "RectangularWindowRadioButton";
            this.RectangularWindowRadioButton.Size = new System.Drawing.Size(155, 21);
            this.RectangularWindowRadioButton.TabIndex = 0;
            this.RectangularWindowRadioButton.TabStop = true;
            this.RectangularWindowRadioButton.Text = "Rectangular window";
            this.RectangularWindowRadioButton.UseVisualStyleBackColor = true;
            this.RectangularWindowRadioButton.CheckedChanged += new System.EventHandler(this.RectangularWindowRadioButton_CheckedChanged);
            // 
            // FrequencyGraphSettings
            // 
            this.FrequencyGraphSettings.Controls.Add(this.FrequencyAnalizingWindowSettings);
            this.FrequencyGraphSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.FrequencyGraphSettings.Location = new System.Drawing.Point(0, 0);
            this.FrequencyGraphSettings.Name = "FrequencyGraphSettings";
            this.FrequencyGraphSettings.Size = new System.Drawing.Size(1268, 100);
            this.FrequencyGraphSettings.TabIndex = 6;
            // 
            // FrequencyAnalizingWindowSettings
            // 
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.AnalizingWindowButton);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphForLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFromLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFrom);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFor);
            this.FrequencyAnalizingWindowSettings.Location = new System.Drawing.Point(3, 3);
            this.FrequencyAnalizingWindowSettings.Name = "FrequencyAnalizingWindowSettings";
            this.FrequencyAnalizingWindowSettings.Size = new System.Drawing.Size(343, 81);
            this.FrequencyAnalizingWindowSettings.TabIndex = 1;
            this.FrequencyAnalizingWindowSettings.TabStop = false;
            this.FrequencyAnalizingWindowSettings.Text = "Set window start/leght";
            // 
            // AnalizingWindowButton
            // 
            this.AnalizingWindowButton.Location = new System.Drawing.Point(287, 22);
            this.AnalizingWindowButton.Name = "AnalizingWindowButton";
            this.AnalizingWindowButton.Size = new System.Drawing.Size(50, 46);
            this.AnalizingWindowButton.TabIndex = 4;
            this.AnalizingWindowButton.Text = "GO";
            this.AnalizingWindowButton.UseVisualStyleBackColor = true;
            this.AnalizingWindowButton.Click += new System.EventHandler(this.AnalizingWindowButton_Click);
            // 
            // FrequencyGraphForLabel
            // 
            this.FrequencyGraphForLabel.AutoSize = true;
            this.FrequencyGraphForLabel.Location = new System.Drawing.Point(13, 49);
            this.FrequencyGraphForLabel.Name = "FrequencyGraphForLabel";
            this.FrequencyGraphForLabel.Size = new System.Drawing.Size(162, 17);
            this.FrequencyGraphForLabel.TabIndex = 3;
            this.FrequencyGraphForLabel.Text = "Analazing window length";
            // 
            // FrequencyGraphFromLabel
            // 
            this.FrequencyGraphFromLabel.AutoSize = true;
            this.FrequencyGraphFromLabel.Location = new System.Drawing.Point(13, 22);
            this.FrequencyGraphFromLabel.Name = "FrequencyGraphFromLabel";
            this.FrequencyGraphFromLabel.Size = new System.Drawing.Size(151, 17);
            this.FrequencyGraphFromLabel.TabIndex = 2;
            this.FrequencyGraphFromLabel.Text = "Analazing window start";
            // 
            // FrequencyGraphFrom
            // 
            this.FrequencyGraphFrom.Location = new System.Drawing.Point(181, 20);
            this.FrequencyGraphFrom.Name = "FrequencyGraphFrom";
            this.FrequencyGraphFrom.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFrom.TabIndex = 1;
            // 
            // FrequencyGraphFor
            // 
            this.FrequencyGraphFor.Location = new System.Drawing.Point(181, 46);
            this.FrequencyGraphFor.Name = "FrequencyGraphFor";
            this.FrequencyGraphFor.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFor.TabIndex = 0;
            // 
            // FrequencyChartsContainer
            // 
            this.FrequencyChartsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrequencyChartsContainer.Controls.Add(this.FrequencyLeftChart);
            this.FrequencyChartsContainer.Controls.Add(this.FrequencyRightChart);
            this.FrequencyChartsContainer.Location = new System.Drawing.Point(0, 100);
            this.FrequencyChartsContainer.Name = "FrequencyChartsContainer";
            this.FrequencyChartsContainer.Size = new System.Drawing.Size(1268, 400);
            this.FrequencyChartsContainer.TabIndex = 3;
            // 
            // FrequencyLeftChart
            // 
            this.FrequencyLeftChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea9.Name = "ChartArea1";
            this.FrequencyLeftChart.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.FrequencyLeftChart.Legends.Add(legend9);
            this.FrequencyLeftChart.Location = new System.Drawing.Point(0, 0);
            this.FrequencyLeftChart.Name = "FrequencyLeftChart";
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Series1";
            this.FrequencyLeftChart.Series.Add(series9);
            this.FrequencyLeftChart.Size = new System.Drawing.Size(1268, 200);
            this.FrequencyLeftChart.TabIndex = 1;
            // 
            // FrequencyRightChart
            // 
            this.FrequencyRightChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea10.Name = "ChartArea1";
            this.FrequencyRightChart.ChartAreas.Add(chartArea10);
            legend10.Name = "Legend1";
            this.FrequencyRightChart.Legends.Add(legend10);
            this.FrequencyRightChart.Location = new System.Drawing.Point(0, 200);
            this.FrequencyRightChart.Name = "FrequencyRightChart";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            this.FrequencyRightChart.Series.Add(series10);
            this.FrequencyRightChart.Size = new System.Drawing.Size(1268, 200);
            this.FrequencyRightChart.TabIndex = 2;
            this.FrequencyRightChart.Text = "chart2";
            // 
            // FrequencyDomainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FrequencyDomainContainer);
            this.Name = "FrequencyDomainControl";
            this.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyDomainContainer.ResumeLayout(false);
            this.FrequencyBottomPanel.ResumeLayout(false);
            this.FrequencyWindowBox.ResumeLayout(false);
            this.FrequencyWindowBox.PerformLayout();
            this.FrequencyGraphSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.PerformLayout();
            this.FrequencyChartsContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyLeftChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyRightChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel FrequencyDomainContainer;
        private System.Windows.Forms.Panel FrequencyBottomPanel;
        private System.Windows.Forms.GroupBox FrequencyWindowBox;
        private System.Windows.Forms.RadioButton BarlettWindowRadioButton;
        private System.Windows.Forms.RadioButton BlackmanWindowRadioButton;
        private System.Windows.Forms.RadioButton HammingWindowRadioButton;
        private System.Windows.Forms.RadioButton HannWindowRadioButton;
        private System.Windows.Forms.RadioButton RectangularWindowRadioButton;
        private System.Windows.Forms.Panel FrequencyGraphSettings;
        private System.Windows.Forms.GroupBox FrequencyAnalizingWindowSettings;
        private System.Windows.Forms.Button AnalizingWindowButton;
        private System.Windows.Forms.Label FrequencyGraphForLabel;
        private System.Windows.Forms.Label FrequencyGraphFromLabel;
        private System.Windows.Forms.TextBox FrequencyGraphFrom;
        private System.Windows.Forms.TextBox FrequencyGraphFor;
        private System.Windows.Forms.Panel FrequencyChartsContainer;
        private System.Windows.Forms.DataVisualization.Charting.Chart FrequencyLeftChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart FrequencyRightChart;
    }
}
