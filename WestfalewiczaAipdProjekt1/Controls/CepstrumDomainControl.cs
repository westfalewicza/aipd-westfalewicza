﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WestfalewiczaAipdProjekt1.Core.Models;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.IntegralTransforms;
using System.Numerics;
using WestfalewiczaAipdProjekt1.Core;
using WestfalewiczaAipdProjekt1.Core.Windows;
using System.Diagnostics;
using WestfalewiczaAipdProjekt1.Core.Services;
using WestfalewiczaAipdProjekt1.Core.FourierTransforms;

namespace WestfalewiczaAipdProjekt1.Controls
{
    public partial class CepstrumDomainControl : UserControl
    {
        public CondencedAudio Audio { get; set; }

        public void Process()
        {
            DrawFrequencies();
        }

        public CepstrumDomainControl()
        {
            InitializeComponent();
        }

        private void RectangularWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HannWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HammingWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BlackmanWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BarlettWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void AnalizingWindowButton_Click(object sender, EventArgs e) => DrawFrequencies();

        private void SetupCharts()
        {
            LeftChart.BackColor = Color.White;
            LeftChart.Series.Clear();
            LeftChart.Legends.Clear();
            LeftChart.Series.Add("FftReal");
            LeftChart.Series[0].ChartType = SeriesChartType.FastLine;

            RightChart.BackColor = Color.White;
            RightChart.Series.Clear();
            RightChart.Legends.Clear();
            RightChart.Series.Add("FftReal");
            RightChart.Series[0].ChartType = SeriesChartType.FastLine;
        }
        
        private void DrawFrequencies()
        {
            if (Audio == null)
                return;

            var (framesL, framesR) = GetFrames();
            SetupCharts();

            ProcessFFT(framesL, LeftChart);
            ProcessFFT(framesR, RightChart);
        }

        private (List<AudioFrame> framesL, List<AudioFrame> framesR) GetFrames()
        {
            if (!int.TryParse(FrequencyGraphFrom.Text, out int secondFrom))
            {
                secondFrom = 0;
                FrequencyGraphFrom.Text = secondFrom.ToString();
            }
            if (!int.TryParse(FrequencyGraphFor.Text, out int secondsFor))
            {
                secondsFor = 10 > Audio.Time ? (int)Audio.Time : 10;
                FrequencyGraphFor.Text = secondsFor.ToString();
            }
            var xd = new RandomTimeAccessAudio(Audio);
            (IEnumerable<AudioFrame>, IEnumerable<AudioFrame>) dx = xd.Frames(secondFrom, secondFrom + secondsFor, 0, Config.CepstrumFrameLength);

            return (dx.Item1.ToList(), dx.Item2.ToList());
        }

        private void ProcessFFT(List<AudioFrame> frames, Chart chart)
        {
            if (frames == null || !frames.Any())
                return;
            
            var processor = new FrequencyAnalizer();
            var baseFrequencies = processor.ProcessCepstrum(frames, Audio.WaveFormat.SampleRate, FourierProxy.Fourier());

            foreach (var kv in baseFrequencies.AsEnumerable())
            {
                chart.Series[0].Points.AddXY(kv.Key, kv.Value);
            }
        }
    }
}
