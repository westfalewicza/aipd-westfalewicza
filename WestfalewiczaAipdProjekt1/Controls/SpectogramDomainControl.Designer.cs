﻿namespace WestfalewiczaAipdProjekt1.Controls
{
    partial class SpectogramDomainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SpectogramDomainContainer = new System.Windows.Forms.Panel();
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.SpectogramSettings = new System.Windows.Forms.Panel();
            this.OverlaySettings = new System.Windows.Forms.GroupBox();
            this.OverlaySettingsChanged = new System.Windows.Forms.Button();
            this.OverlaySettingsLabel = new System.Windows.Forms.Label();
            this.OverlaySetttingInput = new System.Windows.Forms.TextBox();
            this.FrequencyAnalizingWindowSettings = new System.Windows.Forms.GroupBox();
            this.AnalizingWindowButton = new System.Windows.Forms.Button();
            this.FrequencyGraphForLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFromLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFrom = new System.Windows.Forms.TextBox();
            this.FrequencyGraphFor = new System.Windows.Forms.TextBox();
            this.SpectogramsContainer = new System.Windows.Forms.Panel();
            this.RightSpectogram = new System.Windows.Forms.PictureBox();
            this.LeftSpectogram = new System.Windows.Forms.PictureBox();
            this.FrequencyWindowBox = new System.Windows.Forms.GroupBox();
            this.BarlettWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.BlackmanWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HammingWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.HannWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.RectangularWindowRadioButton = new System.Windows.Forms.RadioButton();
            this.SpectogramDomainContainer.SuspendLayout();
            this.BottomPanel.SuspendLayout();
            this.SpectogramSettings.SuspendLayout();
            this.OverlaySettings.SuspendLayout();
            this.FrequencyAnalizingWindowSettings.SuspendLayout();
            this.SpectogramsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightSpectogram)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSpectogram)).BeginInit();
            this.FrequencyWindowBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SpectogramDomainContainer
            // 
            this.SpectogramDomainContainer.Controls.Add(this.BottomPanel);
            this.SpectogramDomainContainer.Controls.Add(this.SpectogramSettings);
            this.SpectogramDomainContainer.Controls.Add(this.SpectogramsContainer);
            this.SpectogramDomainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SpectogramDomainContainer.Location = new System.Drawing.Point(0, 0);
            this.SpectogramDomainContainer.Name = "SpectogramDomainContainer";
            this.SpectogramDomainContainer.Size = new System.Drawing.Size(1268, 859);
            this.SpectogramDomainContainer.TabIndex = 6;
            // 
            // BottomPanel
            // 
            this.BottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BottomPanel.Controls.Add(this.FrequencyWindowBox);
            this.BottomPanel.Location = new System.Drawing.Point(0, 500);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(1268, 359);
            this.BottomPanel.TabIndex = 7;
            // 
            // SpectogramSettings
            // 
            this.SpectogramSettings.Controls.Add(this.OverlaySettings);
            this.SpectogramSettings.Controls.Add(this.FrequencyAnalizingWindowSettings);
            this.SpectogramSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.SpectogramSettings.Location = new System.Drawing.Point(0, 0);
            this.SpectogramSettings.Name = "SpectogramSettings";
            this.SpectogramSettings.Size = new System.Drawing.Size(1268, 100);
            this.SpectogramSettings.TabIndex = 6;
            // 
            // OverlaySettings
            // 
            this.OverlaySettings.Controls.Add(this.OverlaySettingsChanged);
            this.OverlaySettings.Controls.Add(this.OverlaySettingsLabel);
            this.OverlaySettings.Controls.Add(this.OverlaySetttingInput);
            this.OverlaySettings.Location = new System.Drawing.Point(352, 3);
            this.OverlaySettings.Name = "OverlaySettings";
            this.OverlaySettings.Size = new System.Drawing.Size(343, 81);
            this.OverlaySettings.TabIndex = 2;
            this.OverlaySettings.TabStop = false;
            this.OverlaySettings.Text = "Set frames overlay ratio";
            // 
            // OverlaySettingsChanged
            // 
            this.OverlaySettingsChanged.Location = new System.Drawing.Point(287, 22);
            this.OverlaySettingsChanged.Name = "OverlaySettingsChanged";
            this.OverlaySettingsChanged.Size = new System.Drawing.Size(50, 46);
            this.OverlaySettingsChanged.TabIndex = 4;
            this.OverlaySettingsChanged.Text = "GO";
            this.OverlaySettingsChanged.UseVisualStyleBackColor = true;
            this.OverlaySettingsChanged.Click += new System.EventHandler(this.OverlaySettingsChanged_Click);
            // 
            // OverlaySettingsLabel
            // 
            this.OverlaySettingsLabel.AutoSize = true;
            this.OverlaySettingsLabel.Location = new System.Drawing.Point(13, 22);
            this.OverlaySettingsLabel.Name = "OverlaySettingsLabel";
            this.OverlaySettingsLabel.Size = new System.Drawing.Size(187, 17);
            this.OverlaySettingsLabel.TabIndex = 2;
            this.OverlaySettingsLabel.Text = "Frames overlay(percentage)";
            // 
            // OverlaySetttingInput
            // 
            this.OverlaySetttingInput.Location = new System.Drawing.Point(206, 20);
            this.OverlaySetttingInput.Name = "OverlaySetttingInput";
            this.OverlaySetttingInput.Size = new System.Drawing.Size(75, 22);
            this.OverlaySetttingInput.TabIndex = 1;
            // 
            // FrequencyAnalizingWindowSettings
            // 
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.AnalizingWindowButton);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphForLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFromLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFrom);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFor);
            this.FrequencyAnalizingWindowSettings.Location = new System.Drawing.Point(3, 3);
            this.FrequencyAnalizingWindowSettings.Name = "FrequencyAnalizingWindowSettings";
            this.FrequencyAnalizingWindowSettings.Size = new System.Drawing.Size(343, 81);
            this.FrequencyAnalizingWindowSettings.TabIndex = 1;
            this.FrequencyAnalizingWindowSettings.TabStop = false;
            this.FrequencyAnalizingWindowSettings.Text = "Set window start/leght";
            // 
            // AnalizingWindowButton
            // 
            this.AnalizingWindowButton.Location = new System.Drawing.Point(287, 22);
            this.AnalizingWindowButton.Name = "AnalizingWindowButton";
            this.AnalizingWindowButton.Size = new System.Drawing.Size(50, 46);
            this.AnalizingWindowButton.TabIndex = 4;
            this.AnalizingWindowButton.Text = "GO";
            this.AnalizingWindowButton.UseVisualStyleBackColor = true;
            this.AnalizingWindowButton.Click += new System.EventHandler(this.AnalizingWindowButton_Click);
            // 
            // FrequencyGraphForLabel
            // 
            this.FrequencyGraphForLabel.AutoSize = true;
            this.FrequencyGraphForLabel.Location = new System.Drawing.Point(13, 49);
            this.FrequencyGraphForLabel.Name = "FrequencyGraphForLabel";
            this.FrequencyGraphForLabel.Size = new System.Drawing.Size(162, 17);
            this.FrequencyGraphForLabel.TabIndex = 3;
            this.FrequencyGraphForLabel.Text = "Analazing window length";
            // 
            // FrequencyGraphFromLabel
            // 
            this.FrequencyGraphFromLabel.AutoSize = true;
            this.FrequencyGraphFromLabel.Location = new System.Drawing.Point(13, 22);
            this.FrequencyGraphFromLabel.Name = "FrequencyGraphFromLabel";
            this.FrequencyGraphFromLabel.Size = new System.Drawing.Size(151, 17);
            this.FrequencyGraphFromLabel.TabIndex = 2;
            this.FrequencyGraphFromLabel.Text = "Analazing window start";
            // 
            // FrequencyGraphFrom
            // 
            this.FrequencyGraphFrom.Location = new System.Drawing.Point(181, 20);
            this.FrequencyGraphFrom.Name = "FrequencyGraphFrom";
            this.FrequencyGraphFrom.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFrom.TabIndex = 1;
            // 
            // FrequencyGraphFor
            // 
            this.FrequencyGraphFor.Location = new System.Drawing.Point(181, 46);
            this.FrequencyGraphFor.Name = "FrequencyGraphFor";
            this.FrequencyGraphFor.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFor.TabIndex = 0;
            // 
            // SpectogramsContainer
            // 
            this.SpectogramsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SpectogramsContainer.Controls.Add(this.RightSpectogram);
            this.SpectogramsContainer.Controls.Add(this.LeftSpectogram);
            this.SpectogramsContainer.Location = new System.Drawing.Point(0, 100);
            this.SpectogramsContainer.Name = "SpectogramsContainer";
            this.SpectogramsContainer.Size = new System.Drawing.Size(1268, 400);
            this.SpectogramsContainer.TabIndex = 3;
            // 
            // RightSpectogram
            // 
            this.RightSpectogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RightSpectogram.BackColor = System.Drawing.Color.White;
            this.RightSpectogram.Location = new System.Drawing.Point(0, 200);
            this.RightSpectogram.Name = "RightSpectogram";
            this.RightSpectogram.Size = new System.Drawing.Size(1268, 200);
            this.RightSpectogram.TabIndex = 1;
            this.RightSpectogram.TabStop = false;
            // 
            // LeftSpectogram
            // 
            this.LeftSpectogram.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LeftSpectogram.BackColor = System.Drawing.Color.White;
            this.LeftSpectogram.Location = new System.Drawing.Point(0, 0);
            this.LeftSpectogram.Name = "LeftSpectogram";
            this.LeftSpectogram.Size = new System.Drawing.Size(1268, 200);
            this.LeftSpectogram.TabIndex = 0;
            this.LeftSpectogram.TabStop = false;
            // 
            // FrequencyWindowBox
            // 
            this.FrequencyWindowBox.Controls.Add(this.BarlettWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.BlackmanWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HammingWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.HannWindowRadioButton);
            this.FrequencyWindowBox.Controls.Add(this.RectangularWindowRadioButton);
            this.FrequencyWindowBox.Location = new System.Drawing.Point(3, 6);
            this.FrequencyWindowBox.Name = "FrequencyWindowBox";
            this.FrequencyWindowBox.Size = new System.Drawing.Size(200, 163);
            this.FrequencyWindowBox.TabIndex = 1;
            this.FrequencyWindowBox.TabStop = false;
            this.FrequencyWindowBox.Text = "Selection Window";
            // 
            // BarlettWindowRadioButton
            // 
            this.BarlettWindowRadioButton.AutoSize = true;
            this.BarlettWindowRadioButton.Location = new System.Drawing.Point(6, 129);
            this.BarlettWindowRadioButton.Name = "BarlettWindowRadioButton";
            this.BarlettWindowRadioButton.Size = new System.Drawing.Size(119, 21);
            this.BarlettWindowRadioButton.TabIndex = 4;
            this.BarlettWindowRadioButton.Text = "Barlett window";
            this.BarlettWindowRadioButton.UseVisualStyleBackColor = true;
            this.BarlettWindowRadioButton.CheckedChanged += new System.EventHandler(this.BarlettWindowRadioButton_CheckedChanged);
            // 
            // BlackmanWindowRadioButton
            // 
            this.BlackmanWindowRadioButton.AutoSize = true;
            this.BlackmanWindowRadioButton.Location = new System.Drawing.Point(6, 102);
            this.BlackmanWindowRadioButton.Name = "BlackmanWindowRadioButton";
            this.BlackmanWindowRadioButton.Size = new System.Drawing.Size(139, 21);
            this.BlackmanWindowRadioButton.TabIndex = 3;
            this.BlackmanWindowRadioButton.Text = "Blackman window";
            this.BlackmanWindowRadioButton.UseVisualStyleBackColor = true;
            this.BlackmanWindowRadioButton.CheckedChanged += new System.EventHandler(this.BlackmanWindowRadioButton_CheckedChanged);
            // 
            // HammingWindowRadioButton
            // 
            this.HammingWindowRadioButton.AutoSize = true;
            this.HammingWindowRadioButton.Location = new System.Drawing.Point(6, 75);
            this.HammingWindowRadioButton.Name = "HammingWindowRadioButton";
            this.HammingWindowRadioButton.Size = new System.Drawing.Size(137, 21);
            this.HammingWindowRadioButton.TabIndex = 2;
            this.HammingWindowRadioButton.Text = "Hamming window";
            this.HammingWindowRadioButton.UseVisualStyleBackColor = true;
            this.HammingWindowRadioButton.CheckedChanged += new System.EventHandler(this.HammingWindowRadioButton_CheckedChanged);
            // 
            // HannWindowRadioButton
            // 
            this.HannWindowRadioButton.AutoSize = true;
            this.HannWindowRadioButton.Location = new System.Drawing.Point(6, 48);
            this.HannWindowRadioButton.Name = "HannWindowRadioButton";
            this.HannWindowRadioButton.Size = new System.Drawing.Size(112, 21);
            this.HannWindowRadioButton.TabIndex = 1;
            this.HannWindowRadioButton.Text = "Hann window";
            this.HannWindowRadioButton.UseVisualStyleBackColor = true;
            this.HannWindowRadioButton.CheckedChanged += new System.EventHandler(this.HannWindowRadioButton_CheckedChanged);
            // 
            // RectangularWindowRadioButton
            // 
            this.RectangularWindowRadioButton.AutoSize = true;
            this.RectangularWindowRadioButton.Checked = true;
            this.RectangularWindowRadioButton.Location = new System.Drawing.Point(6, 21);
            this.RectangularWindowRadioButton.Name = "RectangularWindowRadioButton";
            this.RectangularWindowRadioButton.Size = new System.Drawing.Size(155, 21);
            this.RectangularWindowRadioButton.TabIndex = 0;
            this.RectangularWindowRadioButton.TabStop = true;
            this.RectangularWindowRadioButton.Text = "Rectangular window";
            this.RectangularWindowRadioButton.UseVisualStyleBackColor = true;
            this.RectangularWindowRadioButton.CheckedChanged += new System.EventHandler(this.RectangularWindowRadioButton_CheckedChanged);
            // 
            // SpectogramControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SpectogramDomainContainer);
            this.Name = "SpectogramControl";
            this.Size = new System.Drawing.Size(1268, 859);
            this.SpectogramDomainContainer.ResumeLayout(false);
            this.BottomPanel.ResumeLayout(false);
            this.SpectogramSettings.ResumeLayout(false);
            this.OverlaySettings.ResumeLayout(false);
            this.OverlaySettings.PerformLayout();
            this.FrequencyAnalizingWindowSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.PerformLayout();
            this.SpectogramsContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RightSpectogram)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSpectogram)).EndInit();
            this.FrequencyWindowBox.ResumeLayout(false);
            this.FrequencyWindowBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel SpectogramDomainContainer;
        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.Panel SpectogramSettings;
        private System.Windows.Forms.GroupBox FrequencyAnalizingWindowSettings;
        private System.Windows.Forms.Button AnalizingWindowButton;
        private System.Windows.Forms.Label FrequencyGraphForLabel;
        private System.Windows.Forms.Label FrequencyGraphFromLabel;
        private System.Windows.Forms.TextBox FrequencyGraphFrom;
        private System.Windows.Forms.TextBox FrequencyGraphFor;
        private System.Windows.Forms.Panel SpectogramsContainer;
        private System.Windows.Forms.PictureBox RightSpectogram;
        private System.Windows.Forms.PictureBox LeftSpectogram;
        private System.Windows.Forms.GroupBox OverlaySettings;
        private System.Windows.Forms.Button OverlaySettingsChanged;
        private System.Windows.Forms.Label OverlaySettingsLabel;
        private System.Windows.Forms.TextBox OverlaySetttingInput;
        private System.Windows.Forms.GroupBox FrequencyWindowBox;
        private System.Windows.Forms.RadioButton BarlettWindowRadioButton;
        private System.Windows.Forms.RadioButton BlackmanWindowRadioButton;
        private System.Windows.Forms.RadioButton HammingWindowRadioButton;
        private System.Windows.Forms.RadioButton HannWindowRadioButton;
        private System.Windows.Forms.RadioButton RectangularWindowRadioButton;
    }
}
