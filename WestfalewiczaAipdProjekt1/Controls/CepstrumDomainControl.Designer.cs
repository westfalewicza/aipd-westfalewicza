﻿namespace WestfalewiczaAipdProjekt1.Controls
{
    partial class CepstrumDomainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.FrequencyDomainContainer = new System.Windows.Forms.Panel();
            this.FrequencyBottomPanel = new System.Windows.Forms.Panel();
            this.FrequencyGraphSettings = new System.Windows.Forms.Panel();
            this.FrequencyAnalizingWindowSettings = new System.Windows.Forms.GroupBox();
            this.AnalizingWindowButton = new System.Windows.Forms.Button();
            this.FrequencyGraphForLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFromLabel = new System.Windows.Forms.Label();
            this.FrequencyGraphFrom = new System.Windows.Forms.TextBox();
            this.FrequencyGraphFor = new System.Windows.Forms.TextBox();
            this.FrequencyChartsContainer = new System.Windows.Forms.Panel();
            this.LeftChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RightChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.FrequencyDomainContainer.SuspendLayout();
            this.FrequencyGraphSettings.SuspendLayout();
            this.FrequencyAnalizingWindowSettings.SuspendLayout();
            this.FrequencyChartsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LeftChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightChart)).BeginInit();
            this.SuspendLayout();
            // 
            // FrequencyDomainContainer
            // 
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyBottomPanel);
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyGraphSettings);
            this.FrequencyDomainContainer.Controls.Add(this.FrequencyChartsContainer);
            this.FrequencyDomainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrequencyDomainContainer.Location = new System.Drawing.Point(0, 0);
            this.FrequencyDomainContainer.Name = "FrequencyDomainContainer";
            this.FrequencyDomainContainer.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyDomainContainer.TabIndex = 6;
            // 
            // FrequencyBottomPanel
            // 
            this.FrequencyBottomPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrequencyBottomPanel.Location = new System.Drawing.Point(0, 500);
            this.FrequencyBottomPanel.Name = "FrequencyBottomPanel";
            this.FrequencyBottomPanel.Size = new System.Drawing.Size(1268, 359);
            this.FrequencyBottomPanel.TabIndex = 7;
            // 
            // FrequencyGraphSettings
            // 
            this.FrequencyGraphSettings.Controls.Add(this.FrequencyAnalizingWindowSettings);
            this.FrequencyGraphSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.FrequencyGraphSettings.Location = new System.Drawing.Point(0, 0);
            this.FrequencyGraphSettings.Name = "FrequencyGraphSettings";
            this.FrequencyGraphSettings.Size = new System.Drawing.Size(1268, 100);
            this.FrequencyGraphSettings.TabIndex = 6;
            // 
            // FrequencyAnalizingWindowSettings
            // 
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.AnalizingWindowButton);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphForLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFromLabel);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFrom);
            this.FrequencyAnalizingWindowSettings.Controls.Add(this.FrequencyGraphFor);
            this.FrequencyAnalizingWindowSettings.Location = new System.Drawing.Point(3, 3);
            this.FrequencyAnalizingWindowSettings.Name = "FrequencyAnalizingWindowSettings";
            this.FrequencyAnalizingWindowSettings.Size = new System.Drawing.Size(343, 81);
            this.FrequencyAnalizingWindowSettings.TabIndex = 1;
            this.FrequencyAnalizingWindowSettings.TabStop = false;
            this.FrequencyAnalizingWindowSettings.Text = "Set window start/leght";
            // 
            // AnalizingWindowButton
            // 
            this.AnalizingWindowButton.Location = new System.Drawing.Point(287, 22);
            this.AnalizingWindowButton.Name = "AnalizingWindowButton";
            this.AnalizingWindowButton.Size = new System.Drawing.Size(50, 46);
            this.AnalizingWindowButton.TabIndex = 4;
            this.AnalizingWindowButton.Text = "GO";
            this.AnalizingWindowButton.UseVisualStyleBackColor = true;
            this.AnalizingWindowButton.Click += new System.EventHandler(this.AnalizingWindowButton_Click);
            // 
            // FrequencyGraphForLabel
            // 
            this.FrequencyGraphForLabel.AutoSize = true;
            this.FrequencyGraphForLabel.Location = new System.Drawing.Point(13, 49);
            this.FrequencyGraphForLabel.Name = "FrequencyGraphForLabel";
            this.FrequencyGraphForLabel.Size = new System.Drawing.Size(162, 17);
            this.FrequencyGraphForLabel.TabIndex = 3;
            this.FrequencyGraphForLabel.Text = "Analazing window length";
            // 
            // FrequencyGraphFromLabel
            // 
            this.FrequencyGraphFromLabel.AutoSize = true;
            this.FrequencyGraphFromLabel.Location = new System.Drawing.Point(13, 22);
            this.FrequencyGraphFromLabel.Name = "FrequencyGraphFromLabel";
            this.FrequencyGraphFromLabel.Size = new System.Drawing.Size(151, 17);
            this.FrequencyGraphFromLabel.TabIndex = 2;
            this.FrequencyGraphFromLabel.Text = "Analazing window start";
            // 
            // FrequencyGraphFrom
            // 
            this.FrequencyGraphFrom.Location = new System.Drawing.Point(181, 20);
            this.FrequencyGraphFrom.Name = "FrequencyGraphFrom";
            this.FrequencyGraphFrom.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFrom.TabIndex = 1;
            // 
            // FrequencyGraphFor
            // 
            this.FrequencyGraphFor.Location = new System.Drawing.Point(181, 46);
            this.FrequencyGraphFor.Name = "FrequencyGraphFor";
            this.FrequencyGraphFor.Size = new System.Drawing.Size(100, 22);
            this.FrequencyGraphFor.TabIndex = 0;
            // 
            // FrequencyChartsContainer
            // 
            this.FrequencyChartsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FrequencyChartsContainer.Controls.Add(this.LeftChart);
            this.FrequencyChartsContainer.Controls.Add(this.RightChart);
            this.FrequencyChartsContainer.Location = new System.Drawing.Point(0, 100);
            this.FrequencyChartsContainer.Name = "FrequencyChartsContainer";
            this.FrequencyChartsContainer.Size = new System.Drawing.Size(1268, 400);
            this.FrequencyChartsContainer.TabIndex = 3;
            // 
            // LeftChart
            // 
            this.LeftChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.LeftChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.LeftChart.Legends.Add(legend1);
            this.LeftChart.Location = new System.Drawing.Point(0, 0);
            this.LeftChart.Name = "LeftChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.LeftChart.Series.Add(series1);
            this.LeftChart.Size = new System.Drawing.Size(1268, 200);
            this.LeftChart.TabIndex = 1;
            // 
            // RightChart
            // 
            this.RightChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.RightChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.RightChart.Legends.Add(legend2);
            this.RightChart.Location = new System.Drawing.Point(0, 200);
            this.RightChart.Name = "RightChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.RightChart.Series.Add(series2);
            this.RightChart.Size = new System.Drawing.Size(1268, 200);
            this.RightChart.TabIndex = 2;
            this.RightChart.Text = "chart2";
            // 
            // CepstrumDomainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FrequencyDomainContainer);
            this.Name = "CepstrumDomainControl";
            this.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyDomainContainer.ResumeLayout(false);
            this.FrequencyGraphSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.ResumeLayout(false);
            this.FrequencyAnalizingWindowSettings.PerformLayout();
            this.FrequencyChartsContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LeftChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel FrequencyDomainContainer;
        private System.Windows.Forms.Panel FrequencyBottomPanel;
        private System.Windows.Forms.Panel FrequencyGraphSettings;
        private System.Windows.Forms.GroupBox FrequencyAnalizingWindowSettings;
        private System.Windows.Forms.Button AnalizingWindowButton;
        private System.Windows.Forms.Label FrequencyGraphForLabel;
        private System.Windows.Forms.Label FrequencyGraphFromLabel;
        private System.Windows.Forms.TextBox FrequencyGraphFrom;
        private System.Windows.Forms.TextBox FrequencyGraphFor;
        private System.Windows.Forms.Panel FrequencyChartsContainer;
        private System.Windows.Forms.DataVisualization.Charting.Chart LeftChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart RightChart;
    }
}
