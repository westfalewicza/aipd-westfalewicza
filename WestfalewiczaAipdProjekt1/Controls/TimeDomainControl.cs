﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WestfalewiczaAipdProjekt1.Core.Models;
using System.Windows.Forms.DataVisualization.Charting;

namespace WestfalewiczaAipdProjekt1.Controls
{
    public partial class TimeDomainControl : UserControl
    {
        public TimeDomainControl()
        {
            InitializeComponent();
        }

        public CondencedAudio Audio { get; set; }
        
        public void Process()
        {
            WaveChartButton.Checked = true;
            DrawWave();
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            LeftChart.Series.Clear();
            LeftChart.Legends.Clear();
            RightChart.Series.Clear();
            RightChart.Legends.Clear();
        }
        
        private void LeftChart_MouseClick(object sender, MouseEventArgs e) => TryDisplayDetails(e.Location, LeftChart);
        private void RightChart_MouseClick(object sender, MouseEventArgs e) => TryDisplayDetails(e.Location, RightChart);

        private void WaveChartButton_CheckedChanged(object sender, EventArgs e) => DrawWave();
        private void SteChartButton_CheckedChanged(object sender, EventArgs e) => DrawSte();
        private void ZcrChartButton_CheckedChanged(object sender, EventArgs e) => DrawZcr();

        private void LeftChart_PrePaint(object sender, ChartPaintEventArgs e) => MarkBackground(sender, e, Audio?.LeftProcessed, Audio?.LeftClips);
        private void RightChart_PrePaint(object sender, ChartPaintEventArgs e) => MarkBackground(sender, e, Audio?.RightProcessed, Audio?.RightClips);

        private void DrawWave()
        {
            if (Audio == null)
                return;
            SetupCharts();

            SeriesHelper(LeftChart, Audio.LeftProcessed.Select(af => af.Start), Audio.LeftProcessed.Select(af => Audio.Left[af.StartIndex]));
            SeriesHelper(RightChart, Audio.RightProcessed.Select(af => af.Start), Audio.RightProcessed.Select(af => Audio.Right[af.StartIndex]));
        }

        private void DrawZcr()
        {
            if (Audio == null)
                return;
            SetupCharts();

            SeriesHelper(LeftChart, Audio.LeftProcessed.Select(af => af.Start), Audio.LeftProcessed.Select(af => af.Zcr));
            SeriesHelper(RightChart, Audio.RightProcessed.Select(af => af.Start), Audio.RightProcessed.Select(af => af.Zcr));
        }

        private void DrawSte()
        {
            if (Audio == null)
                return;
            SetupCharts();

            SeriesHelper(LeftChart, Audio.LeftProcessed.Select(af => af.Start), Audio.LeftProcessed.Select(af => af.Ste));
            SeriesHelper(RightChart, Audio.RightProcessed.Select(af => af.Start), Audio.RightProcessed.Select(af => af.Ste));
        }

        void SeriesHelper(Chart chart, IEnumerable<float> xValues, IEnumerable<float> yValues)
        {
            foreach ((float x, float y) in xValues.Zip(yValues, (x, y) => (x, y)))
            {
                chart.Series[0].Points.AddXY(x, y);
            }

        }

        private void SetupCharts()
        {
                LeftChart.Series.Clear();
                LeftChart.Legends.Clear();
                RightChart.Series.Clear();
                RightChart.Legends.Clear();
                LeftChart.Series.Add("LeftChannel");
                LeftChart.Series[0].ChartType = SeriesChartType.FastLine;
                RightChart.Series.Add("RightChannel");
                RightChart.Series[0].ChartType = SeriesChartType.FastLine;
            
        }

        private void TryDisplayDetails(Point location, Chart chart)
        {
            HitTestResult[] results = chart.HitTest(location.X, location.Y, false, ChartElementType.DataPoint);

            HitTestResult result = results.Where(r => r.ChartElementType == ChartElementType.DataPoint).FirstOrDefault();
            if (result == null)
                return;

            var index = result.PointIndex;
            AudioFrame leftFrame = Audio.LeftProcessed[index];
            AudioFrame rightFrame = Audio.RightProcessed[index];

            var leftfreameIndex = Audio.LeftProcessed.FindIndex(f => f.StartIndex == leftFrame.StartIndex);
            var leftClip = Audio.LeftClips.OrderBy(f => f.StartIndex).First(f => f.StartIndex <= leftfreameIndex && f.EndIndex >= leftfreameIndex);

            var rightfreameIndex = Audio.RightProcessed.FindIndex(f => f.StartIndex == rightFrame.StartIndex);
            var rightClip = Audio.RightClips.OrderBy(f => f.StartIndex).First(f => f.StartIndex <= rightfreameIndex && f.EndIndex >= rightfreameIndex);

            ShowDetails(leftFrame, rightFrame, leftClip, rightClip);
        }

        private void ShowDetails(AudioFrame leftFrame, AudioFrame rightFrame, AudioClip leftClip, AudioClip rightClip)
        {
            LeftSTE.Text = $"STE: {leftFrame.Ste}";
            LeftZCR.Text = $"ZCR: {leftFrame.Zcr}";
            LeftMLER.Text = $"MLE: {leftClip.Mler}";
            LeftSDZCR.Text = $"SDZCR: {leftClip.Sdzcr}";
            RightSTE.Text = $"STE: {rightFrame.Ste}";
            RightZCR.Text = $"ZCR: {rightFrame.Zcr}";
            RightMLER.Text = $"MLE: {rightClip.Mler}";
            RightSDZCR.Text = $"SDZCR: {rightClip.Sdzcr}";
        }

        private void MarkBackground(object sender, ChartPaintEventArgs e, List<AudioFrame> frameCollection, List<AudioClip> clipsCollection)
        {
            if (Audio == null)
                return;

            if (MarkClipsButton.Checked)
            {
                MarkClipsBackground(sender, e, frameCollection, clipsCollection);
            }
            else if (MarkFramesButton.Checked)
            {
                MarkFramesBackground(sender, e, frameCollection, clipsCollection);
            }

        }
        private void MarkClipsBackground(object sender, ChartPaintEventArgs e, List<AudioFrame> frameCollection, List<AudioClip> leftClips)
        {
            foreach (var clip in leftClips)
            {
                double xMax = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.X, clip.End);
                double xMin = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.X, clip.Start);

                double yMax = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.Y, ((Chart)sender).ChartAreas[0].AxisY.Minimum);
                double yMin = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.Y, ((Chart)sender).ChartAreas[0].AxisY.Maximum);
                double width = Math.Abs(xMax - xMin);
                double heigth = Math.Abs(yMax - yMin);
                RectangleF myRect = new RectangleF((float)xMin, (float)yMin, (float)width, (float)heigth);
                myRect = e.ChartGraphics.GetAbsoluteRectangle(myRect);
                switch (clip.Type)
                {
                    case AudioClipType.None:
                        break;
                    case AudioClipType.Music:
                        e.ChartGraphics.Graphics.FillRectangle(new SolidBrush(Color.LightBlue), myRect);
                        break;
                    case AudioClipType.Speech:
                        e.ChartGraphics.Graphics.FillRectangle(new SolidBrush(Color.Pink), myRect);
                        break;
                    default:
                        throw new Exception();
                }
            }
        }
        private void MarkFramesBackground(object sender, ChartPaintEventArgs e, List<AudioFrame> frameCollection, List<AudioClip> leftClips)
        {

            AudioFrame currentBlockStartFrame = frameCollection[0];
            for (int i = 1; i < frameCollection.Count; i++)
            {
                var current = frameCollection[i];
                if (current.FrameType != currentBlockStartFrame.FrameType || i == frameCollection.Count - 1)
                {
                    double xMax = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.X, current.End);
                    double xMin = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.X, currentBlockStartFrame.Start);

                    double yMax = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.Y, ((Chart)sender).ChartAreas[0].AxisY.Minimum);
                    double yMin = e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.Y, ((Chart)sender).ChartAreas[0].AxisY.Maximum);
                    double width = Math.Abs(xMax - xMin);
                    double heigth = Math.Abs(yMax - yMin);
                    RectangleF myRect = new RectangleF((float)xMin, (float)yMin, (float)width, (float)heigth);
                    myRect = e.ChartGraphics.GetAbsoluteRectangle(myRect);
                    switch (currentBlockStartFrame.FrameType)
                    {
                        case AudioFrameType.None:
                            break;
                        case AudioFrameType.Silence:
                            e.ChartGraphics.Graphics.FillRectangle(new SolidBrush(Color.LightGray), myRect);
                            break;
                        case AudioFrameType.Unvoiced:
                            e.ChartGraphics.Graphics.FillRectangle(new SolidBrush(Color.LightBlue), myRect);
                            break;
                        case AudioFrameType.Voiced:
                            e.ChartGraphics.Graphics.FillRectangle(new SolidBrush(Color.Pink), myRect);
                            break;
                        default:
                            throw new Exception();
                    }
                    currentBlockStartFrame = current;
                }
            }
        }

        private void MarkClipsButton_CheckedChanged(object sender, EventArgs e)
        {
            LeftChart.Refresh();
            RightChart.Refresh();
        }

        private void MarkFramesButton_CheckedChanged(object sender, EventArgs e)
        {
            LeftChart.Refresh();
            RightChart.Refresh();
        }
    }
}
