﻿namespace WestfalewiczaAipdProjekt1.Controls
{
    partial class TimeDomainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.TimeDomainContainer = new System.Windows.Forms.Panel();
            this.DetailsContainer = new System.Windows.Forms.Panel();
            this.HighLighterdElements = new System.Windows.Forms.GroupBox();
            this.ColorSilenceFrame = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ColorMusicClip = new System.Windows.Forms.Label();
            this.ColorVoiceClip = new System.Windows.Forms.Label();
            this.LabelNoneClip = new System.Windows.Forms.Label();
            this.LabelMusicClip = new System.Windows.Forms.Label();
            this.LabelVoiceClip = new System.Windows.Forms.Label();
            this.ColorNoneClip = new System.Windows.Forms.Label();
            this.MarkFramesButton = new System.Windows.Forms.RadioButton();
            this.MarkClipsButton = new System.Windows.Forms.RadioButton();
            this.RightChannelDetails = new System.Windows.Forms.GroupBox();
            this.RightSDZCR = new System.Windows.Forms.Label();
            this.RightMLER = new System.Windows.Forms.Label();
            this.RightZCR = new System.Windows.Forms.Label();
            this.RightSTE = new System.Windows.Forms.Label();
            this.LeftChannelDetails = new System.Windows.Forms.GroupBox();
            this.LeftSDZCR = new System.Windows.Forms.Label();
            this.LeftMLER = new System.Windows.Forms.Label();
            this.LeftZCR = new System.Windows.Forms.Label();
            this.LeftSTE = new System.Windows.Forms.Label();
            this.ChartsContainer = new System.Windows.Forms.Panel();
            this.RightChannelLabel = new System.Windows.Forms.Label();
            this.LeftCharLabel = new System.Windows.Forms.Label();
            this.RightChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.LeftChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PickChartContainer = new System.Windows.Forms.Panel();
            this.SteChartButton = new System.Windows.Forms.RadioButton();
            this.ZcrChartButton = new System.Windows.Forms.RadioButton();
            this.WaveChartButton = new System.Windows.Forms.RadioButton();
            this.TimeDomainContainer.SuspendLayout();
            this.DetailsContainer.SuspendLayout();
            this.HighLighterdElements.SuspendLayout();
            this.RightChannelDetails.SuspendLayout();
            this.LeftChannelDetails.SuspendLayout();
            this.ChartsContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftChart)).BeginInit();
            this.PickChartContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // TimeDomainContainer
            // 
            this.TimeDomainContainer.Controls.Add(this.DetailsContainer);
            this.TimeDomainContainer.Controls.Add(this.ChartsContainer);
            this.TimeDomainContainer.Controls.Add(this.PickChartContainer);
            this.TimeDomainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TimeDomainContainer.Location = new System.Drawing.Point(0, 0);
            this.TimeDomainContainer.Name = "TimeDomainContainer";
            this.TimeDomainContainer.Size = new System.Drawing.Size(1268, 859);
            this.TimeDomainContainer.TabIndex = 5;
            // 
            // DetailsContainer
            // 
            this.DetailsContainer.Controls.Add(this.HighLighterdElements);
            this.DetailsContainer.Controls.Add(this.RightChannelDetails);
            this.DetailsContainer.Controls.Add(this.LeftChannelDetails);
            this.DetailsContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DetailsContainer.Location = new System.Drawing.Point(0, 450);
            this.DetailsContainer.Name = "DetailsContainer";
            this.DetailsContainer.Size = new System.Drawing.Size(1268, 409);
            this.DetailsContainer.TabIndex = 3;
            // 
            // HighLighterdElements
            // 
            this.HighLighterdElements.Controls.Add(this.ColorSilenceFrame);
            this.HighLighterdElements.Controls.Add(this.label8);
            this.HighLighterdElements.Controls.Add(this.label1);
            this.HighLighterdElements.Controls.Add(this.label2);
            this.HighLighterdElements.Controls.Add(this.label3);
            this.HighLighterdElements.Controls.Add(this.label4);
            this.HighLighterdElements.Controls.Add(this.label5);
            this.HighLighterdElements.Controls.Add(this.label6);
            this.HighLighterdElements.Controls.Add(this.ColorMusicClip);
            this.HighLighterdElements.Controls.Add(this.ColorVoiceClip);
            this.HighLighterdElements.Controls.Add(this.LabelNoneClip);
            this.HighLighterdElements.Controls.Add(this.LabelMusicClip);
            this.HighLighterdElements.Controls.Add(this.LabelVoiceClip);
            this.HighLighterdElements.Controls.Add(this.ColorNoneClip);
            this.HighLighterdElements.Controls.Add(this.MarkFramesButton);
            this.HighLighterdElements.Controls.Add(this.MarkClipsButton);
            this.HighLighterdElements.Location = new System.Drawing.Point(461, 6);
            this.HighLighterdElements.Name = "HighLighterdElements";
            this.HighLighterdElements.Size = new System.Drawing.Size(417, 165);
            this.HighLighterdElements.TabIndex = 3;
            this.HighLighterdElements.TabStop = false;
            this.HighLighterdElements.Text = "Chose what is marked on charts";
            // 
            // ColorSilenceFrame
            // 
            this.ColorSilenceFrame.BackColor = System.Drawing.Color.LightGray;
            this.ColorSilenceFrame.Location = new System.Drawing.Point(316, 90);
            this.ColorSilenceFrame.Name = "ColorSilenceFrame";
            this.ColorSilenceFrame.Size = new System.Drawing.Size(30, 17);
            this.ColorSilenceFrame.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(352, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Silence";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Pink;
            this.label1.Location = new System.Drawing.Point(211, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.LightBlue;
            this.label2.Location = new System.Drawing.Point(316, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(247, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Unknown";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(247, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Voiced";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(352, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Unvoiced";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(211, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 17);
            this.label6.TabIndex = 8;
            // 
            // ColorMusicClip
            // 
            this.ColorMusicClip.BackColor = System.Drawing.Color.Pink;
            this.ColorMusicClip.Location = new System.Drawing.Point(11, 92);
            this.ColorMusicClip.Name = "ColorMusicClip";
            this.ColorMusicClip.Size = new System.Drawing.Size(30, 17);
            this.ColorMusicClip.TabIndex = 7;
            // 
            // ColorVoiceClip
            // 
            this.ColorVoiceClip.BackColor = System.Drawing.Color.LightBlue;
            this.ColorVoiceClip.Location = new System.Drawing.Point(11, 126);
            this.ColorVoiceClip.Name = "ColorVoiceClip";
            this.ColorVoiceClip.Size = new System.Drawing.Size(30, 17);
            this.ColorVoiceClip.TabIndex = 6;
            // 
            // LabelNoneClip
            // 
            this.LabelNoneClip.AutoSize = true;
            this.LabelNoneClip.Location = new System.Drawing.Point(47, 65);
            this.LabelNoneClip.Name = "LabelNoneClip";
            this.LabelNoneClip.Size = new System.Drawing.Size(66, 17);
            this.LabelNoneClip.TabIndex = 5;
            this.LabelNoneClip.Text = "Unknown";
            // 
            // LabelMusicClip
            // 
            this.LabelMusicClip.AutoSize = true;
            this.LabelMusicClip.Location = new System.Drawing.Point(47, 92);
            this.LabelMusicClip.Name = "LabelMusicClip";
            this.LabelMusicClip.Size = new System.Drawing.Size(56, 17);
            this.LabelMusicClip.TabIndex = 4;
            this.LabelMusicClip.Text = "Speech";
            // 
            // LabelVoiceClip
            // 
            this.LabelVoiceClip.AutoSize = true;
            this.LabelVoiceClip.Location = new System.Drawing.Point(47, 126);
            this.LabelVoiceClip.Name = "LabelVoiceClip";
            this.LabelVoiceClip.Size = new System.Drawing.Size(44, 17);
            this.LabelVoiceClip.TabIndex = 3;
            this.LabelVoiceClip.Text = "Music";
            // 
            // ColorNoneClip
            // 
            this.ColorNoneClip.BackColor = System.Drawing.Color.White;
            this.ColorNoneClip.Location = new System.Drawing.Point(11, 65);
            this.ColorNoneClip.Name = "ColorNoneClip";
            this.ColorNoneClip.Size = new System.Drawing.Size(30, 17);
            this.ColorNoneClip.TabIndex = 2;
            // 
            // MarkFramesButton
            // 
            this.MarkFramesButton.AutoSize = true;
            this.MarkFramesButton.Location = new System.Drawing.Point(208, 31);
            this.MarkFramesButton.Name = "MarkFramesButton";
            this.MarkFramesButton.Size = new System.Drawing.Size(201, 21);
            this.MarkFramesButton.TabIndex = 1;
            this.MarkFramesButton.TabStop = true;
            this.MarkFramesButton.Text = "Mark frame types on charts";
            this.MarkFramesButton.UseVisualStyleBackColor = true;
            this.MarkFramesButton.CheckedChanged += new System.EventHandler(this.MarkFramesButton_CheckedChanged);
            // 
            // MarkClipsButton
            // 
            this.MarkClipsButton.AutoSize = true;
            this.MarkClipsButton.Location = new System.Drawing.Point(14, 30);
            this.MarkClipsButton.Name = "MarkClipsButton";
            this.MarkClipsButton.Size = new System.Drawing.Size(188, 21);
            this.MarkClipsButton.TabIndex = 0;
            this.MarkClipsButton.TabStop = true;
            this.MarkClipsButton.Text = "Mark Clip types on charts";
            this.MarkClipsButton.UseVisualStyleBackColor = true;
            this.MarkClipsButton.CheckedChanged += new System.EventHandler(this.MarkClipsButton_CheckedChanged);
            // 
            // RightChannelDetails
            // 
            this.RightChannelDetails.Controls.Add(this.RightSDZCR);
            this.RightChannelDetails.Controls.Add(this.RightMLER);
            this.RightChannelDetails.Controls.Add(this.RightZCR);
            this.RightChannelDetails.Controls.Add(this.RightSTE);
            this.RightChannelDetails.Location = new System.Drawing.Point(237, 6);
            this.RightChannelDetails.Name = "RightChannelDetails";
            this.RightChannelDetails.Size = new System.Drawing.Size(200, 165);
            this.RightChannelDetails.TabIndex = 2;
            this.RightChannelDetails.TabStop = false;
            this.RightChannelDetails.Text = "Right Channel Details";
            // 
            // RightSDZCR
            // 
            this.RightSDZCR.AutoSize = true;
            this.RightSDZCR.Location = new System.Drawing.Point(6, 126);
            this.RightSDZCR.Name = "RightSDZCR";
            this.RightSDZCR.Size = new System.Drawing.Size(165, 17);
            this.RightSDZCR.TabIndex = 3;
            this.RightSDZCR.Text = "Pick a clip to see SDZCR";
            // 
            // RightMLER
            // 
            this.RightMLER.AutoSize = true;
            this.RightMLER.Location = new System.Drawing.Point(6, 92);
            this.RightMLER.Name = "RightMLER";
            this.RightMLER.Size = new System.Drawing.Size(156, 17);
            this.RightMLER.TabIndex = 2;
            this.RightMLER.Text = "Pick a clip to see MLER";
            // 
            // RightZCR
            // 
            this.RightZCR.AutoSize = true;
            this.RightZCR.Location = new System.Drawing.Point(6, 65);
            this.RightZCR.Name = "RightZCR";
            this.RightZCR.Size = new System.Drawing.Size(161, 17);
            this.RightZCR.TabIndex = 1;
            this.RightZCR.Text = "Pick a frame to see ZCR";
            // 
            // RightSTE
            // 
            this.RightSTE.AutoSize = true;
            this.RightSTE.Location = new System.Drawing.Point(6, 31);
            this.RightSTE.Name = "RightSTE";
            this.RightSTE.Size = new System.Drawing.Size(160, 17);
            this.RightSTE.TabIndex = 0;
            this.RightSTE.Text = "Pick a frame to see STE";
            // 
            // LeftChannelDetails
            // 
            this.LeftChannelDetails.Controls.Add(this.LeftSDZCR);
            this.LeftChannelDetails.Controls.Add(this.LeftMLER);
            this.LeftChannelDetails.Controls.Add(this.LeftZCR);
            this.LeftChannelDetails.Controls.Add(this.LeftSTE);
            this.LeftChannelDetails.Location = new System.Drawing.Point(12, 6);
            this.LeftChannelDetails.Name = "LeftChannelDetails";
            this.LeftChannelDetails.Size = new System.Drawing.Size(200, 165);
            this.LeftChannelDetails.TabIndex = 0;
            this.LeftChannelDetails.TabStop = false;
            this.LeftChannelDetails.Text = "Left Channel Details";
            // 
            // LeftSDZCR
            // 
            this.LeftSDZCR.AutoSize = true;
            this.LeftSDZCR.Location = new System.Drawing.Point(8, 126);
            this.LeftSDZCR.Name = "LeftSDZCR";
            this.LeftSDZCR.Size = new System.Drawing.Size(165, 17);
            this.LeftSDZCR.TabIndex = 3;
            this.LeftSDZCR.Text = "Pick a clip to see SDZCR";
            // 
            // LeftMLER
            // 
            this.LeftMLER.AutoSize = true;
            this.LeftMLER.Location = new System.Drawing.Point(8, 92);
            this.LeftMLER.Name = "LeftMLER";
            this.LeftMLER.Size = new System.Drawing.Size(156, 17);
            this.LeftMLER.TabIndex = 2;
            this.LeftMLER.Text = "Pick a clip to see MLER";
            // 
            // LeftZCR
            // 
            this.LeftZCR.AutoSize = true;
            this.LeftZCR.Location = new System.Drawing.Point(7, 65);
            this.LeftZCR.Name = "LeftZCR";
            this.LeftZCR.Size = new System.Drawing.Size(161, 17);
            this.LeftZCR.TabIndex = 1;
            this.LeftZCR.Text = "Pick a frame to see ZCR";
            // 
            // LeftSTE
            // 
            this.LeftSTE.AutoSize = true;
            this.LeftSTE.Location = new System.Drawing.Point(8, 31);
            this.LeftSTE.Name = "LeftSTE";
            this.LeftSTE.Size = new System.Drawing.Size(160, 17);
            this.LeftSTE.TabIndex = 0;
            this.LeftSTE.Text = "Pick a frame to see STE";
            // 
            // ChartsContainer
            // 
            this.ChartsContainer.Controls.Add(this.RightChannelLabel);
            this.ChartsContainer.Controls.Add(this.LeftCharLabel);
            this.ChartsContainer.Controls.Add(this.RightChart);
            this.ChartsContainer.Controls.Add(this.LeftChart);
            this.ChartsContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.ChartsContainer.Location = new System.Drawing.Point(0, 50);
            this.ChartsContainer.Name = "ChartsContainer";
            this.ChartsContainer.Size = new System.Drawing.Size(1268, 400);
            this.ChartsContainer.TabIndex = 3;
            // 
            // RightChannelLabel
            // 
            this.RightChannelLabel.AutoSize = true;
            this.RightChannelLabel.BackColor = System.Drawing.Color.White;
            this.RightChannelLabel.Location = new System.Drawing.Point(20, 220);
            this.RightChannelLabel.Name = "RightChannelLabel";
            this.RightChannelLabel.Size = new System.Drawing.Size(97, 17);
            this.RightChannelLabel.TabIndex = 3;
            this.RightChannelLabel.Text = "Right Channel";
            // 
            // LeftCharLabel
            // 
            this.LeftCharLabel.AutoSize = true;
            this.LeftCharLabel.BackColor = System.Drawing.Color.White;
            this.LeftCharLabel.Location = new System.Drawing.Point(20, 20);
            this.LeftCharLabel.Name = "LeftCharLabel";
            this.LeftCharLabel.Size = new System.Drawing.Size(88, 17);
            this.LeftCharLabel.TabIndex = 2;
            this.LeftCharLabel.Text = "Left Channel";
            // 
            // RightChart
            // 
            chartArea3.Name = "ChartArea1";
            this.RightChart.ChartAreas.Add(chartArea3);
            this.RightChart.Dock = System.Windows.Forms.DockStyle.Top;
            legend3.Name = "Legend1";
            this.RightChart.Legends.Add(legend3);
            this.RightChart.Location = new System.Drawing.Point(0, 200);
            this.RightChart.Name = "RightChart";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.RightChart.Series.Add(series3);
            this.RightChart.Size = new System.Drawing.Size(1268, 200);
            this.RightChart.TabIndex = 1;
            this.RightChart.Text = "Right Channel";
            this.RightChart.PrePaint += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs>(this.RightChart_PrePaint);
            this.RightChart.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.RightChart_MouseClick);
            // 
            // LeftChart
            // 
            chartArea4.Name = "ChartArea1";
            this.LeftChart.ChartAreas.Add(chartArea4);
            this.LeftChart.Dock = System.Windows.Forms.DockStyle.Top;
            legend4.Name = "Legend1";
            this.LeftChart.Legends.Add(legend4);
            this.LeftChart.Location = new System.Drawing.Point(0, 0);
            this.LeftChart.Name = "LeftChart";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.LeftChart.Series.Add(series4);
            this.LeftChart.Size = new System.Drawing.Size(1268, 200);
            this.LeftChart.TabIndex = 0;
            this.LeftChart.Text = "Left Channel";
            this.LeftChart.PrePaint += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs>(this.LeftChart_PrePaint);
            this.LeftChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LeftChart_MouseClick);
            // 
            // PickChartContainer
            // 
            this.PickChartContainer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.PickChartContainer.Controls.Add(this.SteChartButton);
            this.PickChartContainer.Controls.Add(this.ZcrChartButton);
            this.PickChartContainer.Controls.Add(this.WaveChartButton);
            this.PickChartContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.PickChartContainer.Location = new System.Drawing.Point(0, 0);
            this.PickChartContainer.Name = "PickChartContainer";
            this.PickChartContainer.Size = new System.Drawing.Size(1268, 50);
            this.PickChartContainer.TabIndex = 2;
            // 
            // SteChartButton
            // 
            this.SteChartButton.AutoSize = true;
            this.SteChartButton.Location = new System.Drawing.Point(78, 15);
            this.SteChartButton.Name = "SteChartButton";
            this.SteChartButton.Size = new System.Drawing.Size(56, 21);
            this.SteChartButton.TabIndex = 4;
            this.SteChartButton.Text = "STE";
            this.SteChartButton.UseVisualStyleBackColor = true;
            this.SteChartButton.CheckedChanged += new System.EventHandler(this.SteChartButton_CheckedChanged);
            // 
            // ZcrChartButton
            // 
            this.ZcrChartButton.AutoSize = true;
            this.ZcrChartButton.Location = new System.Drawing.Point(149, 15);
            this.ZcrChartButton.Name = "ZcrChartButton";
            this.ZcrChartButton.Size = new System.Drawing.Size(57, 21);
            this.ZcrChartButton.TabIndex = 2;
            this.ZcrChartButton.Text = "ZCR";
            this.ZcrChartButton.UseVisualStyleBackColor = true;
            this.ZcrChartButton.CheckedChanged += new System.EventHandler(this.ZcrChartButton_CheckedChanged);
            // 
            // WaveChartButton
            // 
            this.WaveChartButton.AutoSize = true;
            this.WaveChartButton.Checked = true;
            this.WaveChartButton.Location = new System.Drawing.Point(7, 15);
            this.WaveChartButton.Name = "WaveChartButton";
            this.WaveChartButton.Size = new System.Drawing.Size(65, 21);
            this.WaveChartButton.TabIndex = 0;
            this.WaveChartButton.TabStop = true;
            this.WaveChartButton.Text = "Wave";
            this.WaveChartButton.UseVisualStyleBackColor = true;
            this.WaveChartButton.CheckedChanged += new System.EventHandler(this.WaveChartButton_CheckedChanged);
            // 
            // TimeDomainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TimeDomainContainer);
            this.Name = "TimeDomainControl";
            this.Size = new System.Drawing.Size(1268, 859);
            this.TimeDomainContainer.ResumeLayout(false);
            this.DetailsContainer.ResumeLayout(false);
            this.HighLighterdElements.ResumeLayout(false);
            this.HighLighterdElements.PerformLayout();
            this.RightChannelDetails.ResumeLayout(false);
            this.RightChannelDetails.PerformLayout();
            this.LeftChannelDetails.ResumeLayout(false);
            this.LeftChannelDetails.PerformLayout();
            this.ChartsContainer.ResumeLayout(false);
            this.ChartsContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RightChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftChart)).EndInit();
            this.PickChartContainer.ResumeLayout(false);
            this.PickChartContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TimeDomainContainer;
        private System.Windows.Forms.Panel DetailsContainer;
        private System.Windows.Forms.GroupBox HighLighterdElements;
        private System.Windows.Forms.Label ColorSilenceFrame;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label ColorMusicClip;
        private System.Windows.Forms.Label ColorVoiceClip;
        private System.Windows.Forms.Label LabelNoneClip;
        private System.Windows.Forms.Label LabelMusicClip;
        private System.Windows.Forms.Label LabelVoiceClip;
        private System.Windows.Forms.Label ColorNoneClip;
        private System.Windows.Forms.RadioButton MarkFramesButton;
        private System.Windows.Forms.RadioButton MarkClipsButton;
        private System.Windows.Forms.GroupBox RightChannelDetails;
        private System.Windows.Forms.Label RightSDZCR;
        private System.Windows.Forms.Label RightMLER;
        private System.Windows.Forms.Label RightZCR;
        private System.Windows.Forms.Label RightSTE;
        private System.Windows.Forms.GroupBox LeftChannelDetails;
        private System.Windows.Forms.Label LeftSDZCR;
        private System.Windows.Forms.Label LeftMLER;
        private System.Windows.Forms.Label LeftZCR;
        private System.Windows.Forms.Label LeftSTE;
        private System.Windows.Forms.Panel ChartsContainer;
        private System.Windows.Forms.Label RightChannelLabel;
        private System.Windows.Forms.Label LeftCharLabel;
        private System.Windows.Forms.DataVisualization.Charting.Chart RightChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart LeftChart;
        private System.Windows.Forms.Panel PickChartContainer;
        private System.Windows.Forms.RadioButton SteChartButton;
        private System.Windows.Forms.RadioButton ZcrChartButton;
        private System.Windows.Forms.RadioButton WaveChartButton;
    }
}
