﻿using MathNet.Numerics.IntegralTransforms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using WestfalewiczaAipdProjekt1.Core;
using WestfalewiczaAipdProjekt1.Core.FourierTransforms;
using WestfalewiczaAipdProjekt1.Core.Models;
using WestfalewiczaAipdProjekt1.Core.Services;
using WestfalewiczaAipdProjekt1.Core.Windows;

namespace WestfalewiczaAipdProjekt1.Controls
{
    public partial class SpectogramDomainControl : UserControl
    {
        public CondencedAudio Audio { get; set; }

        public SpectogramDomainControl()
        {
            InitializeComponent();
        }

        private void RectangularWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HannWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HammingWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BlackmanWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BarlettWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void AnalizingWindowButton_Click(object sender, EventArgs e) => DrawSpectogram();
        private void OverlaySettingsChanged_Click(object sender, EventArgs e) => DrawSpectogram();

        public void Process()
        {
            DrawSpectogram();
        }

        private void DrawSpectogram()
        {
            if (Audio == null)
                return;

            var (framesL, framesR) = GetFrames();
            DoDrawSpectogram(LeftSpectogram, framesL.ToList());
            DoDrawSpectogram(RightSpectogram, framesR.ToList());
        }

        private void DoDrawSpectogram(PictureBox picture, List<AudioFrame> frames)
        {
            if (frames == null || !frames.Any())
                return;

            var processor = new FrequencyAnalizer();
            List<Complex[]> transformed = processor.ProcessSpectogram(frames, Audio.WaveFormat.SampleRate, GetFrequencyWindow(frames.Count), FourierProxy.Fourier());

            Bitmap bmp = DrawSpectrogram(transformed);
            picture.BackgroundImage = bmp;
        }

        private (IEnumerable<AudioFrame> framesL, IEnumerable<AudioFrame> framesR) GetFrames()
        {
            if (!int.TryParse(FrequencyGraphFrom.Text, out int secondFrom))
            {
                secondFrom = 0;
                FrequencyGraphFrom.Text = secondFrom.ToString();
            }
            if (!int.TryParse(FrequencyGraphFor.Text, out int secondsFor))
            {
                secondsFor = 10 > Audio.Time ? (int)Audio.Time : 10;
                FrequencyGraphFor.Text = secondsFor.ToString();
            }
            if (!int.TryParse(OverlaySetttingInput.Text, out int overlay) || overlay < 0 || overlay > 100)
            {
                overlay = 0;
                OverlaySetttingInput.Text = overlay.ToString();
            }
            var frameLength = Audio.LeftProcessed.First().Data.Count;

            var xd = new RandomTimeAccessAudio(Audio);
            return xd.Frames(secondFrom, secondsFor, overlay, frameLength);
        }

        public Bitmap DrawSpectrogram(List<Complex[]> input)
        {
            var data = CutOffZeros(input);

            Size sz = new Size(LeftSpectogram.Size.Width, LeftSpectogram.Size.Height);
            Bitmap bmp = new Bitmap(sz.Width, sz.Height);

            int xRange = data.Count;
            int yRange = data[0].Length;

            float stepX = (float)sz.Width / (float)xRange;
            float stepY = (float)sz.Height / (float)yRange;

            double Max = data.Select(t => t.Max(x => x.Magnitude)).Max();
            using (Graphics G = Graphics.FromImage(bmp))
            {
                for (int x = 0; x < xRange; x++)
                {
                    for (int y = 0; y < yRange; y++)
                    {
                        double val = (data[x][y].Magnitude / Max);
                        using (SolidBrush brush = new SolidBrush(ValueToColor(val)))
                        {
                            var rect = new RectangleF(x * stepX, (yRange - y) * stepY, stepX, stepY);
                            G.FillRectangle(brush, rect);
                        }
                    }
                }
            }
            return bmp;

            Color ValueToColor(double value)
            {
                int temp = (int)(255 * (1 - value) * (1 - value) * (1 - value));
                return Color.FromArgb(255, temp, temp, temp);
            }
        }

        private List<Complex[]> CutOffZeros(List<Complex[]> data)
        {
            int onlyZerosAfter = data[0].Length;
            for (int i = 0; i < data[0].Length; i++)
            {
                bool onlyZeros = true;
                for (int j = 0; j < data.Count; j++)
                {
                    if (data[j][i].Magnitude >= Config.PracticallyZeroFrequency)
                    {
                        onlyZeros = false;
                        break;
                    }
                }
                if (onlyZeros)
                {
                    onlyZerosAfter = i;
                    break;
                }
            }
            return data.Select(list => list.Take(onlyZerosAfter).ToArray()).ToList();
        }

        private AbstractWindow GetFrequencyWindow(int windowSampleCount)
        {
            if (BarlettWindowRadioButton.Checked)
                return new BarlettWindow(windowSampleCount);
            if (BlackmanWindowRadioButton.Checked)
                return new BlackmanWindow(windowSampleCount);
            if (HammingWindowRadioButton.Checked)
                return new HammingWindow(windowSampleCount);
            if (HannWindowRadioButton.Checked)
                return new HannWindow(windowSampleCount);
            if (RectangularWindowRadioButton.Checked)
                return new RectangularWindow(windowSampleCount);

            throw new Exception();
        }
    }
}
