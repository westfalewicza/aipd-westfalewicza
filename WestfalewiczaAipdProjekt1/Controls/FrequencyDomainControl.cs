﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WestfalewiczaAipdProjekt1.Core.Models;
using System.Windows.Forms.DataVisualization.Charting;
using MathNet.Numerics.IntegralTransforms;
using System.Numerics;
using WestfalewiczaAipdProjekt1.Core;
using WestfalewiczaAipdProjekt1.Core.Windows;
using System.Diagnostics;
using WestfalewiczaAipdProjekt1.Core.FourierTransforms;
using WestfalewiczaAipdProjekt1.Core.Services;

namespace WestfalewiczaAipdProjekt1.Controls
{
    public partial class FrequencyDomainControl : UserControl
    {
        public CondencedAudio Audio { get; set; }

        public void Process()
        {
            DrawFrequencies();
        }

        public FrequencyDomainControl()
        {
            InitializeComponent();
        }

        private void RectangularWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HannWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void HammingWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BlackmanWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void BarlettWindowRadioButton_CheckedChanged(object sender, EventArgs e) => AnalizingWindowButton_Click(null, null);
        private void AnalizingWindowButton_Click(object sender, EventArgs e) => DrawFrequencies();

        private void SetupCharts(AudioDomain domain = AudioDomain.Time)
        {
            FrequencyLeftChart.BackColor = Color.White;
            FrequencyLeftChart.Series.Clear();
            FrequencyLeftChart.Legends.Clear();
            FrequencyLeftChart.Series.Add("FftReal");
            FrequencyLeftChart.Series.Add("FftImg");
            FrequencyLeftChart.Series[0].ChartType = SeriesChartType.FastLine;
            FrequencyLeftChart.Series[1].ChartType = SeriesChartType.FastLine;

            FrequencyRightChart.BackColor = Color.White;
            FrequencyRightChart.Series.Clear();
            FrequencyRightChart.Legends.Clear();
            FrequencyRightChart.Series.Add("FftReal");
            FrequencyRightChart.Series.Add("FftImg");
            FrequencyRightChart.Series[0].ChartType = SeriesChartType.FastLine;
            FrequencyRightChart.Series[1].ChartType = SeriesChartType.FastLine;
        }

        private void DrawFrequencies()
        {
            if (Audio == null)
                return;

            var (framesL, framesR) = GetFrames();
            SetupCharts(AudioDomain.Frequency);
            AbstractWindow window = GetFrequencyWindow(framesL.Count);

            ProcessFFT(framesL, FrequencyLeftChart, window);
            ProcessFFT(framesR, FrequencyRightChart, window);
        }

        private (List<AudioFrame> framesL, List<AudioFrame> framesR) GetFrames()
        {
            if (!int.TryParse(FrequencyGraphFrom.Text, out int secondFrom))
            {
                secondFrom = 0;
                FrequencyGraphFrom.Text = secondFrom.ToString();
            }
            if (!int.TryParse(FrequencyGraphFor.Text, out int secondsFor))
            {
                secondsFor = 10 > Audio.Time ? (int)Audio.Time : 10;
                FrequencyGraphFor.Text = secondsFor.ToString();
            }
            var frameLength = Audio.LeftProcessed.First().Data.Count;

            var framesL = Audio.LeftProcessed.Where(f => f.Start >= secondFrom &&
                                        f.End <= secondFrom + secondsFor &&
                                        f.Data.Count == frameLength).ToList();
            var framesR = Audio.RightProcessed.Where(f => f.Start >= secondFrom &&
                                        f.End <= secondFrom + secondsFor &&
                                        f.Data.Count == frameLength).ToList();
            return (framesL, framesR);
        }

        private AbstractWindow GetFrequencyWindow(int windowSampleCount)
        {
            if (BarlettWindowRadioButton.Checked)
                return new BarlettWindow(windowSampleCount);
            if (BlackmanWindowRadioButton.Checked)
                return new BlackmanWindow(windowSampleCount);
            if (HammingWindowRadioButton.Checked)
                return new HammingWindow(windowSampleCount);
            if (HannWindowRadioButton.Checked)
                return new HannWindow(windowSampleCount);
            if (RectangularWindowRadioButton.Checked)
                return new RectangularWindow(windowSampleCount);

            throw new Exception();
        }

        private void ProcessFFT(List<AudioFrame> frames, Chart chart, AbstractWindow window)
        {
            if (frames == null || !frames.Any())
                return;

            var processor = new FrequencyAnalizer();
            var values = processor.ProcessFFT(frames, Audio.WaveFormat.SampleRate, FourierProxy.Fourier(), window);

            var averages = values.Select(kv => (kv.Key, kv.Value.Average(c => c.Real), kv.Value.Average(c => c.Imaginary)));
            foreach (var avg in averages)
            {
                chart.Series["FftReal"].Points.AddXY(avg.Item1, avg.Item2);
                chart.Series["FftImg"].Points.AddXY(avg.Item1, avg.Item3);
            }
        }
    }
}
