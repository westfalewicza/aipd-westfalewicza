﻿using System;

namespace WestfalewiczaAipdProjekt1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.domainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frequencyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spectogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FileNameInMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.FrequencyDomain = new WestfalewiczaAipdProjekt1.Controls.FrequencyDomainControl();
            this.TimeDomain = new WestfalewiczaAipdProjekt1.Controls.TimeDomainControl();
            this.SpectogramDomain = new WestfalewiczaAipdProjekt1.Controls.SpectogramDomainControl();
            this.CepstrumDomain = new WestfalewiczaAipdProjekt1.Controls.CepstrumDomainControl();
            this.cepstrumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.domainToolStripMenuItem,
            this.FileNameInMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1268, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(117, 26);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // domainToolStripMenuItem
            // 
            this.domainToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.timeToolStripMenuItem,
            this.frequencyToolStripMenuItem,
            this.spectogramToolStripMenuItem,
            this.cepstrumToolStripMenuItem});
            this.domainToolStripMenuItem.Name = "domainToolStripMenuItem";
            this.domainToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.domainToolStripMenuItem.Text = "Domain";
            // 
            // timeToolStripMenuItem
            // 
            this.timeToolStripMenuItem.Name = "timeToolStripMenuItem";
            this.timeToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.timeToolStripMenuItem.Text = "Time";
            this.timeToolStripMenuItem.Click += new System.EventHandler(this.timeToolStripMenuItem_Click);
            // 
            // frequencyToolStripMenuItem
            // 
            this.frequencyToolStripMenuItem.Name = "frequencyToolStripMenuItem";
            this.frequencyToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.frequencyToolStripMenuItem.Text = "Frequency";
            this.frequencyToolStripMenuItem.Click += new System.EventHandler(this.frequencyToolStripMenuItem_Click);
            // 
            // spectogramToolStripMenuItem
            // 
            this.spectogramToolStripMenuItem.Name = "spectogramToolStripMenuItem";
            this.spectogramToolStripMenuItem.Size = new System.Drawing.Size(165, 26);
            this.spectogramToolStripMenuItem.Text = "Spectogram";
            this.spectogramToolStripMenuItem.Click += new System.EventHandler(this.spectogramToolStripMenuItem_Click);
            // 
            // FileNameInMenu
            // 
            this.FileNameInMenu.Name = "FileNameInMenu";
            this.FileNameInMenu.Size = new System.Drawing.Size(171, 24);
            this.FileNameInMenu.Text = "Loaded: no file loaded";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FrequencyDomain
            // 
            this.FrequencyDomain.Audio = null;
            this.FrequencyDomain.Location = new System.Drawing.Point(0, 28);
            this.FrequencyDomain.Name = "FrequencyDomain";
            this.FrequencyDomain.Size = new System.Drawing.Size(1268, 859);
            this.FrequencyDomain.TabIndex = 3;
            // 
            // TimeDomain
            // 
            this.TimeDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeDomain.Audio = null;
            this.TimeDomain.Location = new System.Drawing.Point(0, 28);
            this.TimeDomain.Name = "TimeDomain";
            this.TimeDomain.Size = new System.Drawing.Size(1268, 859);
            this.TimeDomain.TabIndex = 2;
            // 
            // SpectogramDomain
            // 
            this.SpectogramDomain.Audio = null;
            this.SpectogramDomain.Location = new System.Drawing.Point(0, 28);
            this.SpectogramDomain.Name = "SpectogramDomain";
            this.SpectogramDomain.Size = new System.Drawing.Size(1268, 859);
            this.SpectogramDomain.TabIndex = 4;
            // 
            // CepstrumDomain
            // 
            this.CepstrumDomain.Audio = null;
            this.CepstrumDomain.Location = new System.Drawing.Point(0, 28);
            this.CepstrumDomain.Name = "CepstrumDomain";
            this.CepstrumDomain.Size = new System.Drawing.Size(1268, 859);
            this.CepstrumDomain.TabIndex = 5;
            // 
            // cepstrumToolStripMenuItem
            // 
            this.cepstrumToolStripMenuItem.Name = "cepstrumToolStripMenuItem";
            this.cepstrumToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.cepstrumToolStripMenuItem.Text = "Cepstrum";
            this.cepstrumToolStripMenuItem.Click += new System.EventHandler(this.cepstrumToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 887);
            this.Controls.Add(this.CepstrumDomain);
            this.Controls.Add(this.SpectogramDomain);
            this.Controls.Add(this.FrequencyDomain);
            this.Controls.Add(this.TimeDomain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem domainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem frequencyToolStripMenuItem;
        private Controls.TimeDomainControl TimeDomain;
        private System.Windows.Forms.ToolStripMenuItem FileNameInMenu;
        private Controls.FrequencyDomainControl FrequencyDomain;
        private Controls.SpectogramDomainControl SpectogramDomain;
        private System.Windows.Forms.ToolStripMenuItem spectogramToolStripMenuItem;
        private Controls.CepstrumDomainControl CepstrumDomain;
        private System.Windows.Forms.ToolStripMenuItem cepstrumToolStripMenuItem;
    }
}

