﻿namespace WestfalewiczaAipdProjekt1
{
    public enum AudioDomain
    {
        None=0,
        Time = 1,
        Frequency = 2,
        Spectogram = 3,
        Cepstrum =4,
    }
}